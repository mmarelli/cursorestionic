'use strict';

// Define module
module.exports = (app, model) => {

  /**
   * Define access for each database method
   * 
   *  For none
   *    []
   * 
   *  For Averybody
   *    ['all']
   * 
   *  For a specific profile
   *    ['<profileName>','<profileName>','<profileName>']
   *
   *  For a non authenticated user
   *    [...,'anonymous',...]
   * 
   *  For complex context
   *  {
   *    profile: <profileName>, (inclouding all and anonymous)
   *    status: <statusValue>,
   *    type: <statusValue>,
   *    <otherField>: <otherValue>
   *  }
   * 
   */
  model.access = {
    search: ['all'],
    select: ['all'],
    create: ['all'],
    update: ['all'],
    delete: ['all'],
    selectField: [],
    updateField: [],
    process: [],
    cancel: [],
    disable: [],
    enable: [],
    error: [],
  };

  /**
   * Define fields
   */
  model.fields = {
    id: {
      type: app.db.DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: app.db.DataTypes.UUIDV4,
      viewReadOnly: true,
      viewHidden: false,
      viewPattern: null,
      viewType: 'hidden'
    },
    parentId: {
      type: app.db.DataTypes.UUID,
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'relation',
      viewParent: 'parent'
    },
    name: {
      type: app.db.DataTypes.STRING(50),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: '^[A-Za-z0-9]{1,}.+',
      viewType: 'text'
    },
    date: {
      type: app.db.DataTypes.DATE,
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'date'
    },
    number: {
      type: app.db.DataTypes.INTEGER(3),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'number'
    },
    amount: {
      type: app.db.DataTypes.DECIMAL(12,2),
      allowNull: false,
      defaultValue: 0,
      get() {
        return parseFloat(this.getDataValue('amount') || '0');
      },
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'amount'
    },
    image: {
      type: app.db.DataTypes.STRING(50),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'avatar'
    },
    file: {
      type: app.db.DataTypes.STRING(50),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'file'
    },
    virtual: {
      type: app.db.DataTypes.VIRTUAL,
      get() {
        return "Vritual value: " + this.name;
      },
      set(value) {
        throw new Error('Do not try to set the virtual value!');
      },
      viewReadOnly: true,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    type: {
      type: app.db.DataTypes.ENUM('type1', 'type2'),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values'
    },
    status: {
      type: app.db.DataTypes.ENUM('pending', 'process', 'enable', 'disable', 'cancel', 'error'),
      allowNull: false,
      defaultValue: 'pending',
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values'
    },
    notes: {
      type: app.db.DataTypes.TEXT,
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'textarea'
    },
    createdAt: {
      type: app.db.DataTypes.DATE,
      allowNull: false,
      viewHidden: true
    },
    updatedAt: {
      type: app.db.DataTypes.DATE,
      allowNull: false,
      viewHidden: true
    },
  };

  /**
   * Define indexes
   */
  model.indexes = [{
    name: 'name',
    fields: ['name'],
    unique: false
  }, {
    name: 'type',
    fields: ['type'],
    unique: false
  }, {
    name: 'status',
    fields: ['status'],
    unique: false
  }];

  /**
   * Define hook
   * on before load
   *
   * @param {Object} result - Result
   * @return {void}
   */
  model.beforeLoad = (result) => {

    // Define belongs To
    result.belongsTo(app.models.parents, {as: 'parent', foreignKey: 'parentId'});

    // Define Has meny to meny
    result.belongsToMany(app.models.clids, {through: app.models.entitiesChilds});

    // Define has many
    result.hasMany(app.models.clids);


  };

  /**
   * Define hook
   * on before find
   *
   * @param {Object} result - Result
   * @return {Promise}
   */
  model.beforeFind = (result) => {

    // Verify search context view
    if (result.contextView == 'search') {
      result.attributes = ['id', 'status', 'name', 'image', 'createdAt', 'updatedAt'];
      result.include = [];
      // references      
      result.include.push({
        model: app.models.parents,
        as: 'parent',
        attributes: ['id', 'name', 'image'],
        // context
//        where: ['administrator'].indexOf((result.contextUser || {}).access) > -1 ? undefined : {id: result.contextUser.id}
      });
      // chlids      
      result.include.push({
        model: app.models.childs,
        attributes: ['id', 'name', 'image']
      });


    }

    // Verify select context view
    if (result.contextView == 'select') {
      result.include = result.include || [];
      // references      
      result.include.push({
        model: app.models.parents,
        as: 'parent',
        attributes: ['id', 'name', 'image'],
        // context
//        where: ['administrator'].indexOf((result.contextUser || {}).access) > -1 ? undefined : {id: result.contextUser.id}
      });
    }
  };

  /**
   * Define hook
   * on before save
   *
   * @param {Object} result - Result
   * @param {Object} options - Options
   * @return {Promise}
   */
  model.beforeSave = (result, options) => {
    // Default values

    // context
    if (!result.userId && options.contextUser && options.contextUser.id) {
      result.userId = options.contextUser.id;
    }

    // Validations


  };

  /**
   * Define hook
   * on after save
   *
   * @param {Object} result - Result
   * @param {Object} options - Options
   * @return {Promise}
   */
  model.afterSave = (result, options) => {

    // Uplad files and images

  };

  
};
