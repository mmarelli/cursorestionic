'use strict';

// Define module
module.exports = (app, middleware) => {

  /**
   *  config
   * 
   *  https://json-to-js.com/
   * 
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.config = () => {
    return async (req, res, next) => {
      try {
        var model = req.data.model;
        var action = req.data.action;
        var result = null;
        var fields = null;
        var access = null;
        var modelCapitalize = capitalize(camelCaseToWords(model));
        var modelSingularize = singularize(camelCaseToWords(model,true));
        if (app.models[model] && app.models[model].fields) {
          var fields = Object.values(app.models[model].fields);
        }
        if (app.models[model] && app.models[model].access) {
          var access = app.models[model].access;
        }

        if (action == 'pageFields') {

          result = ''
          fields.forEach((field) => {
            if (!field.viewHidden) {
              result += ' ' + field.field + ': {' + "\n"
              result += '  value: ' + (typeof field.defaultValue === 'object' || typeof field.defaultValue === 'undefined' ? 'null' : "'" + field.defaultValue + "'") + ",\n"
              if (field.viewType == 'values') {
                result += "  match: ["
                if (field.type && field.type.values) {
                  result += "'" + field.type.values.join("','") + "'"
                }
                result += "],\n"
                result += '  filter: true' + "\n"
              } else {
                result += '  match: ' + (typeof field.viewPattern === 'object' || typeof field.viewPattern === 'undefined' ? 'null' : "'" + field.viewPattern + "'") + "\n"
              }
              result += ' },' + "\n"
            }
          })
          res.setHeader('Content-Type', 'application/text');

        } else if (action == 'pageForm') {

          result = '<ion-col size-xs="12" size-md="6">' + "\n"
          fields.forEach((field) => {
            let required = '';
            if (!field.allowNull) {
              required = 'required="true"';
            }
            if (field.viewHidden || field.viewType == 'hidden') {

            } else if (field.viewType == 'image') {
              result += '<app-item-image [root]="view" name="image" type="avatar" size="large"' + required + '></app-item-image>' + "\n"
            } else if (field.viewType == 'file') {
              result += '<app-item-file [root]="view" name="file" type="pdf"' + required + '></app-item-file>' + "\n"
            } else if (field.viewType == 'relation') {
              result += '<app-item-relation [root]="view" name="' + field.field + '" from="' + field.viewParent + '" iconType="avatar" max="1" min="1" (onChange)="view.form.patchValue({' + field.field + ': $event.result && $event.result[0] ? $event.result[0].id : \'\'})"' + required + '></app-item-relation>' + "\n"
            } else {
              result += '<app-item [root]="view" name="' + field.field + '" type="' + field.viewType + '"' + required + '></app-item>' + "\n";
            }
          })
          result += '</ion-col>' + "\n"

        } else if (action == 'pageGrid') {

          result = '<ion-row *ngIf="list.list && list.list.length">' + "\n"
          result += '<ion-col size="0.5" class="ion-text-center">' + "\n"
          result += '</ion-col>' + "\n"
          let i = 0
          fields.forEach((field) => {
            if (field.viewHidden || field.viewType == 'hidden') {

            } else {
              i++
              // status-{{item.status}}
              result += '<ion-col' + (i > 3 ? ' class="ion-hide-md-down"' : '') + '>' + "\n"
              result += '{{ page.code + \'.fields.' + field.field + '.label\' | translate }}' + "\n"
              result += '</ion-col>' + "\n"
            }
          });

          result += '</ion-row>' + "\n"
          result += '<ion-row (click)="gotoPage(\'' + model + '/select/:id\', {id: item.id})" *ngFor="let item of list.list">' + "\n"

          result += '<ion-col size="0.5" class="ion-text-center">' + "\n"
          result += '<ion-icon name="eye-outline"></ion-icon>' + "\n"
          result += '</ion-col>' + "\n"
          i = 0
          fields.forEach((field) => {
            if (field.viewHidden || field.viewType == 'hidden') {

            } else {
              i++
              result += '<ion-col' + (i > 3 ? ' class="ion-hide-md-down"' : '') + '>' + "\n"
              if (field.viewType == 'date') {
                result += '{{ item.' + field.field + ' | amDateFormat:\'l\' }}' + "\n"
              } else if (field.viewType == 'time') {
                result += '{{ item.' + field.field + ' | amDateFormat:\'lll\' }}' + "\n"
              } else if (field.viewType == 'number') {
                result += '{{ item.' + field.field + ' | formatNumber }}' + "\n"
              } else if (field.viewType == 'amount') {
                result += '$ {{ item.' + field.field + ' | formatNumber }}' + "\n"
              } else if (field.viewType == 'number') {
                result += '{{ item.' + field.field + ' | formatNumber }} %' + "\n"
              } else if (field.viewType == 'tel') {
                result += '{{ item.' + field.field + ' | formatPhone }}' + "\n"
              } else if (field.viewType == 'email') {
                result += '<a [href]="\'mailto:\'item.' + field.field + '" *ngIf="item.' + field.field + '">{{ item.' + field.field + ' | formatEmail }}</a>' + "\n"
              } else if (field.viewType == 'url') {
                result += '<a [href]="item.' + field.field + '" *ngIf="item.' + field.field + '">{{ item.' + field.field + ' | formatLink }}</a>' + "\n"
              } else if (field.viewType == 'values') {
                result += '{{ page.code + \'.values.' + field.field + '.\' + item.' + field.field + ' | translate }}' + "\n"
              } else if (field.viewType == 'relation') {
                result += '{{ item.' + singularize(field.viewParent) + '.name }}' + "\n"
              } else {
                result += '{{ item.' + field.field + ' }}' + "\n"
              }
              result += '</ion-col>' + "\n"
            }
          });

          result += '</ion-row>' + "\n"

        } else if (action == 'config') {
          result = `
              /* `+ model + ` page */
              {
                path: '`+ model + `/search',
                name: '`+ model + `.search.title',
                icon: 'call',
                menu: [],
                auth: [`+ "'" + access.search.join("','") + "'" + `],
                load: () => import('./pages/`+ model + `/` + model + `.module').then(m => m.` + modelCapitalize + `PageModule)
              },
              {
                path: '`+ model + `/select/:id',
                name: '`+ model + `.select.title',
                icon: 'search',
                menu: [],
                auth: [`+ "'" + access.select.join("','") + "'" + `],
                load: () => import('./pages/`+ model + `/` + model + `.module').then(m => m.` + modelCapitalize + `PageModule)
              },
              {
                path: '`+ model + `/create',
                name: '`+ model + `.create.title',
                icon: 'create',
                menu: [],
                auth: [`+ "'" + access.create.join("','") + "'" + `],
                load: () => import('./pages/`+ model + `/` + model + `.module').then(m => m.` + modelCapitalize + `PageModule)
              },
              {
                path: '`+ model + `/update/:id',
                name: '`+ model + `.update.title',
                icon: 'create',
                menu: [],
                auth: [`+ "'" + access.update.join("','") + "'" + `],
                load: () => import('./pages/`+ model + `/` + model + `.module').then(m => m.` + modelCapitalize + `PageModule)
              },
              {
                path: '`+ model + `/delete/:id',
                name: '`+ model + `.delete.title',
                icon: 'trash',
                menu: [],
                auth: [`+ "'" + access.delete.join("','") + "'" + `],
                load: () => import('./pages/`+ model + `/` + model + `.module').then(m => m.` + modelCapitalize + `PageModule)
              },
              {
                path: '`+ model + `/enable/:id',
                name: '`+ model + `.enable.title',
                icon: 'checkmark',
                menu: [],
                auth: [`+ "'" + access.enable.join("','") + "'" + `],
                load: () => import('./pages/`+ model + `/` + model + `.module').then(m => m.` + modelCapitalize + `PageModule)
              },
              {
                path: '`+ model + `/disable/:id',
                name: '`+ model + `.disable.title',
                icon: 'close',
                menu: [],
                auth: [`+ "'" + access.delete.join("','") + "'" + `],
                load: () => import('./pages/`+ model + `/` + model + `.module').then(m => m.` + modelCapitalize + `PageModule)
              },
              `;
              result=result.replace("['']",'').replace("[''\n]",'')
              res.setHeader('Content-Type', 'application/text');
    
        } else if (action == 'i18n') {

          result = `
          "` + model + `": 
          {
              "search": {
                "title": "` + modelCapitalize + `",
                "input": "Search in ` + modelCapitalize.toLowerCase() + `",
                "empty": "No ` + modelCapitalize.toLowerCase() + ` found"
              },
              "select": {
                "title": "` + modelSingularize + `"
              },
              "create": {
                "title": "Create ` + modelSingularize + `",
                "submit": "Save",
                "cancel": "Cancel"
              },
              "update": {
                "title": "Edit ` + modelSingularize + `",
                "submit": "Save",
                "cancel": "Cancel"
              },
              "delete": {
                "title": "Delete ` + modelSingularize + `",
                "legend": "Do you really want to delete the ` + modelSingularize.toLowerCase() + ` <strong> {{name}} </strong> and all its related data? This action cannot be reversed.",
                "submit": "Delete",
                "cancel": "Cancel"
              },
              "groups": {
                "group1": "Main Info",
                "group2": "Additional Info"
              },
              "fields": {
                `;

          fields.forEach((field) => {
            if (!field.viewHidden) {
              let fieldName = camelCaseToWords(field.field.replace('Id',''));
              
              result += `"` + field.field + `": {
                      "label": "`+ fieldName + `",
                      "invalid": "`+ fieldName + ` is invalid",
                      "required": "`+ fieldName + ` is required"
                    },
                    `;
            }
          });

          result += `},
              "values": {`+ "\n";
          fields.forEach((field) => {
            if (field.type && field.type.values) {
              result += `  "` + field.field + `": {` + "\n";
              field.type.values.forEach((value, i) => {
                result += `   "` + value + `": "` + capitalize(value) + "\"" + ( i < field.type.values.length-1 ? ',' : '' ) + "\n";
              })
              result += `  },` + "\n";
            }
          });
          result += ` },` + "\n";
          result += `}` + "\n";

          result = result.replace(",\n}","\n}").replace(",\n }","\n }").replace(",\n  }","\n  }")
          res.setHeader('Content-Type', 'application/json');
  
        }
        res.status(200).send(result);

      } catch (error) {
        next(error);
      }
    };
  };


};

function singularize(word) {
  const endings = {
    ves: 'fe',
    ies: 'y',
    i: 'us',
    zes: '',
    ses: '',
    es: '',
    s: ''
  };
  return word.replace(
    new RegExp(`(${Object.keys(endings).join('|')})$`),
    r => endings[r]
  );
}

function capitalize(word) {
  return word.charAt(0).toUpperCase() + word.slice(1);
}

var camelCaseToWords = function(str, toSigular = false){
  return str.match(/^[a-z]+|[A-Z][a-z]*/g).map(function(x){
      if(toSigular) {
        x = singularize(x)
      }
      return x[0].toUpperCase() + x.substr(1).toLowerCase();
  }).join(' ');
};

