'use strict';

// Define module
module.exports = (app, middleware) => {

  /**
   * Upload file
   *
   * @return {Function}
   */
  middleware.uploadFile = () => {
    return (req, res, next) => {
      try {

        // Define pathå
        let path = app.config.varPath + '/files';
        if (!app.helpers.fs.existsSync(path)) {
          app.helpers.fs.mkdirSync(path);
          app.helpers.fs.chmodSync(path, 0o775);
        }

        // Define upload function
        let upload = app.helpers.multer({
          storage: app.helpers.multer.diskStorage({
            destination: (req, file, cb) => {
              cb(null, path);
            },
            filename: (req, file, cb) => {
              cb(null, app.helpers.crypto.createHash('md5').update(new Date().toISOString()).digest('hex') + '.' + require('mime-types').extension(file.mimetype));
            }
          })
        }).single('file');

        // Return result
        upload(req, res, (error) => {
          if (req.file && app.helpers.fs.existsSync(req.file.path)) {
            req.data = req.data || {};
            req.data.file = req.file;
            req.data.file.base64 = new Buffer.from(app.helpers.fs.readFileSync(req.file.path)).toString('base64');
          }
          next();
        });
      } catch (error) {
        next(error);
      }
    };
  };
};
