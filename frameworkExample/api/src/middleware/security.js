'use strict';

// Define module
module.exports = (app, middleware) => {

  /**
   * Auth
   *
   * @param {Array} access
   * @return {Function}
   */
  middleware.auth = (access, model, action) => {
    return (req, res, next) => {
      try {
        if (!access) {
          access = [];
        }
        if (model && model.access && model.access[action] && model.access[action].length) {
          access = access.concat(model.access[action]);
        }

        // Define context user
        let contextUser = null;

        // Define token
        let token = req.body.token || req.query.token || req.headers['x-access-token'] || null;
        if (token) {
          try {
            let value = app.helpers.jsonwebtoken.verify(token, app.config.security.token.secret);
            if (value && value.type) {
              contextUser = value;
            }
          } catch (error) {
            if (error && error.message == 'jwt expired') {
              return next(new Error('401 users.errors.expiredToken'));
            }
            if (error && error.message == 'jwt must be provided') {
              return next(new Error('401 users.errors.requiredToken'));
            }
            if (error) {
              return next(new Error('401 users.errors.invalidToken'));
            }
          }
        }

        // Verify access
        if (access.indexOf('all') == -1 && !(access && access.length > 0 && contextUser && contextUser.type == 'root') && access.indexOf((contextUser || {}).type || 'anonymous') == -1) {
          return next(new Error('401 users.errors.invalidAccess'));
        }

        // Define data
        req.data = {
          ...req.body || {},
          ...req.query || {},
          ...req.params || {},
          ...{ contextUser: contextUser }
        };

        // Return result
        next();
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Profile
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.profile = (model) => {
    return async (req, res, next) => {
      try {

        // Verify parameters
        if (!model || !req.data) {
          return next(new Error('400 users.errors.invalidParameters'));
        }

        // Define parameters
        let params = req.data;

        // Verify parameter: context user
        if (!params.contextUser || !params.contextUser.id) {
          return next(new Error('401 users.errors.invalidUsername'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'profile';

        // Define where
        options.where = [{
          id: params.contextUser.id
        }];

        // Define record
        let record = await model.findOne(options);
        if (!record) {
          return next(new Error('401 users.errors.invalidUsername'));
        }

        // Define record name
        if (params.name !== undefined) {
          record.name = params.name || null;
        }

        // Define record image
        if (params.image !== undefined) {
          record.image = params.image || null;
        }

        // Define record phone
        if (params.phone !== undefined) {
          record.phone = params.phone || null;
        }

        // Define record email
        if (params.email !== undefined) {
          record.email = params.email || null;
        }

        // Define record email
        if (params.username !== undefined) {
          record.username = params.username || null;
        }

        // Define record password
        if (params.oldPassword !== undefined && params.newPassword !== undefined) {
          if (!app.helpers.bcryptjs.compareSync(params.oldPassword, record.password)) {
            return next(new Error('401 users.errors.INVALID_PASSWORD'));
          }
          result.password = params.newPassword;
          delete params.newPassword;
          delete params.oldPassword;
        }

        // Define record browser token
        if (params.browserToken !== undefined) {
          record.browserToken = params.browserToken || null;
        }

        // Define record mobile token
        if (params.mobileToken !== undefined) {
          record.mobileToken = params.mobileToken || null;
        }

        // Define record updated at
        record.updatedAt = null;

        // Update record
        await record.save();

        // Define result
        let result = record.get();

        // Delete result fields
        delete result.password;
        delete result.recovery;
        delete result.browserToken;
        delete result.mobileToken;
        delete result.createdAt;
        delete result.updatedAt;
        delete result.deletedAt;

        // Define result token expire
        result.exp = new Date().getTime() + (parseInt(app.config.security.expire || '3600') * 1000);

        // Define result token string
        result.token = app.helpers.jsonwebtoken.sign(result, app.config.security.token.secret);

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Signout
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.signout = (model) => {
    return async (req, res, next) => {
      try {

        // Verify parameters
        let params = req.data;
        if (!model || !params) {
          return next(new Error('400 users.errors.invalidParameters'));
        }

        // Define record
        let result = {
          token: null
        };

        // Define data
        req.data = {
          ...{ contextUser: null }
        };

        // Return result
        res.send({ token: null });
        //next();
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Signin
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.signin = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data || {};

        // Verify parameter: username
        if (!params.username) {
          return next(new Error('400 users.errors.requiredUsername'));
        }

        // Verify parameter: password
        if (!params.password) {
          return next(new Error('400 users.errors.requiredPassword'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'signin';

        // Define where
        options.where = [{
          [params.username.indexOf('@') == -1 ? 'username' : 'email']: params.username.toLowerCase().trim()
        }];

        // Define exists
        let exists = await model.findOne(options);
        if (!exists || !exists.password || !app.helpers.bcryptjs.compareSync(params.password, exists.password)) {
          return next(new Error('401 users.errors.invalidUsernameOrPassword'));
        }

        // Define data
        req.data = {
          ...{ contextUser: { id: exists.id } }
        };

        // Define result
        next();
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Signup
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.signup = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data || {};

        // Verify parameter: username
        if (!params.username) {
          return next(new Error('400 users.errors.requiredUsername'));
        }

        // Verify parameter: password
        if (!params.password) {
          return next(new Error('400 users.errors.requiredPassword'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'sigup';

        // Define where
        options.where = [{
          [params.username.indexOf('@') == -1 ? 'username' : 'email']: params.username.toLowerCase().trim()
        }];

        // Define exists
        let exists = await model.findOne(options);
        if (exists) {
          return next(new Error('401 users.errors.DUPLICATE_EMAIL'));
        }

        // Create record
        let record = await model.create(params);

        // Define data
        req.data = {
          ...{ contextUser: { id: record.id } }
        };

        // Define result
        next();
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Forgot
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.forgot = (model) => {
    return async (req, res, next) => {
      try {

        // Verify model
        if (!model || !req.data) {
          return next(new Error('400 users.errors.invalidParameters'));
        }

        // Define parameters
        let params = req.data;

        // Verify parameter: username
        if (!params.username) {
          return next(new Error('400 users.errors.requiredUsername'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'forgot';

        // Define where
        options.where = [{
          [params.username.indexOf('@') == -1 ? 'username' : 'email']: params.username.toLowerCase().trim()
        }];

        // Define result
        let result = null;

        // Define record
        let record = await model.findOne(options);

        // Define result step 1
        if (record) {

          // Define verify
          let verify = parseInt(app.helpers.crypto.randomBytes(4).toString('hex'), 16).toString().substr(0, 10);

          // Update record password verify
          record.recovery = verify;

          // Update record
          await record.save();

          // Define email
          //await app.helpers.utility.sendEmail({
          //  to: record.name + ' <' + record.email + '>',
          //  subject: 'Recuperar contraseña',
          //  title: '',
          //  color: '',
          //  image: '',
          //  header: {
          //    text: record.name + ',<br> ya podés cambiar tu contraseña ingresando el código de verificación: <strong>' + verify + '</strong>.'
          //  },
          //  footer: {
          //    text: ''
          //  },
          //  action: {
          //    text: '',
          //    href: ''
          //  }
          //});
        }

        // Return result
        next();
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Verify
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.verify = (model) => {
    return async (req, res, next) => {
      try {

        // Verify parameters
        let params = req.data;
        if (!model || !params) {
          return next(new Error('400 users.errors.invalidParameters'));
        }

        // Verify parameter: username
        if (!params.username) {
          return next(new Error('400 users.errors.requiredUsername'));
        }

        // Verify parameter: password
        if (!params.password) {
          return next(new Error('400 users.errors.requiredPassword'));
        }

        // Verify parameter: recovery
        if (!params.recovery) {
          return next(new Error('400 users.errors.requiredRecovery'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'signin';

        // Define where
        options.where = [{
          email: params.username.toLowerCase().trim()
        }];

        // Define result
        let result = null;

        // Define record
        let record = await model.findOne(options);

        // Define result step 2
        if (record) {

          // Verify record password
          if (!app.helpers.bcryptjs.compareSync(params.recovery, record.recovery)) {
            return next(new Error('401 users.errors.invalidRecovery'));
          }

          // Update record password verify
          record.password = params.newPassword;

          // Update record password verify
          record.recovery = null;

          // Define result
          result = await middleware.profile({
            contextUser: {
              id: record.id
            }
          });
        }
        // Return result
        next();
      } catch (error) {
        next(error);
      }
    };
  };
};
