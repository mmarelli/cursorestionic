'use strict';

// Define module
module.exports = (app, middleware) => {

  /**
   * Search
   *
   * - Where filter
   *
   *   A where filter specifies a set of logical conditions to match, similar to
   *   a WHERE clause in a SQL query.
   *
   *   Rest: ?filter[field][op]=<value>
   *   Node: {filter: {where: {<field>: {<op>: <value>}}}}
   *
   * - Order filter
   *
   *   An order filter specifies how to sort the results: ascending (ASC) or
   *   descending (DESC) based on the specified property.
   *
   *   Rest: ?filter[order][field]=<asc|desc>
   *   Node: {filter: {order: {<field>: <asc|desc>}}}
   *
   * - Limit filter
   *
   *   A limit filter limits the number of records returned to the specified
   *   number (or less).
   *
   *   Rest: ?filter[limit]=<value>
   *   Node: {filter: {limit: <value>}}
   *
   * - Offset filter
   *
   *   A skip filter omits the specified number of returned records. This is
   *   useful, for example, to paginate responses.
   *
   *   Rest: ?filter[offset]=<value>
   *   Node: {filter: {offset: <value>}}
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.search = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params) {
          return next(new Error('400 database.search.invalidParameters'));
        }

        // Define options
        let options = {
          where: [],
          order: [],
          include: []
        };

        // Verify filter: where
        if (params.filter && params.filter.where && app.helpers.lodash.isPlainObject(params.filter.where)) {
          for (let f in params.filter.where) {
            let conditions = [];
            for (let field of f.trim().split(',')) {

              let value = params.filter.where[f];
              if (field.match(/\./)) {
                // TODO MAX: buscar por referenca y sus campos para validar
                field = app.helpers.sequelize.col(field);
              } else {
                let found = false;
                for (let modelField in model.fields) {
                  if (modelField == field) {
                    found = true;
                    break;
                  }
                }
                if (!found) {
                  continue;
                }
                field = app.helpers.sequelize.col(model.name + '.' + field);
              }
              if (value && app.helpers.lodash.isString(value)) {
                value = { 'eq': value };
              }
              if (value && app.helpers.lodash.isPlainObject(value)) {
                for (let q in value) {
                  if (app.helpers.sequelize.Op[q]) {
                    conditions.push({ ['$' + field.col + '$']: { [app.helpers.sequelize.Op[q]]: value[q] } });
                  }
                }
              }
            }
            if (conditions.length > 1) {
              options.where.push({ [app.helpers.sequelize.Op.or]: conditions });
            } else if (conditions.length == 1) {
              options.where.push(conditions[0]);
            }
          }
        }

        // Verify filter: order
        if (params.filter && params.filter.order && app.helpers.lodash.isPlainObject(params.filter.order)) {
          for (let f in params.filter.order) {
            let field = f.trim();
            let value = params.filter.order[f];
            let found = false;

            if (field.match(/\./)) {
              // TODO MAX: buscar por referenca y sus campos para validar
              field = app.helpers.sequelize.col(field);
            } else {
              for (let modelField in model.fields) {
                if (modelField == field && model.fields[modelField].type.toString() != app.db.DataTypes.VIRTUAL.toString()) {
                  found = true;
                  break;
                }
              }
              if (!found) {
                continue;
              }
              field = app.helpers.sequelize.col(model.name + '.' + field);
            }
            if (value === '1' || value === 'true' || value === true || value === 'asc' || value === 'ASC') {
              value = 'ASC';
            } else {
              value = 'DESC';
            }
            options.order.push([field, value]);
          }
        }

        // Verify filter: query
        if (params.filter && params.filter.query && app.helpers.lodash.isString(params.filter.query)) {
          options.query = params.filter.query;
        }

        // Verify filter: extra
        if (params.filter && params.filter.extra && app.helpers.lodash.isPlainObject(params.filter.extra)) {
          options.extra = params.filter.extra;
        }

        // Verify filter: limit
        if (params.filter && params.filter.limit) {
          options.limit = app.helpers.lodash.toInteger(params.filter.limit);
        }

        // Verify filter: offset
        if (params.filter && params.filter.offset) {
          options.offset = app.helpers.lodash.toInteger(params.filter.offset);
        }

        // Define table hint
        options.tableHint = app.helpers.sequelize.TableHints.NOLOCK;

        // Define context view
        options.contextView = 'search';

        // Define context user
        options.contextUser = params.contextUser || {};

        // Define result
        let result = await model.findAll(options) || [];

        // Return result
        res.send(result);
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Select
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.select = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.select.invalidParameters'));
        }

        // Define options
        let options = {};

        // Verify filter: offset
        if (params.filter && params.filter.extra && app.helpers.lodash.isPlainObject(params.filter.extra)) {
          options.extra = params.filter.extra;
        }

        // Define context view
        options.contextView = 'select';

        // Define context user
        options.contextUser = params.contextUser || {};

        // Define where
        options.where = [{
          id: params.id
        }];

        // Define result
        let result = await model.findOne(options);
        if (!result || !result.id) {
          return next(new Error('404 database.select.notFound'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Delete
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.delete = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.delete.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context view
        options.contextView = 'delete';

        // Define context user
        options.contextUser = params.contextUser || {};

        // Define where
        options.where = [{
          id: params.id
        }];

        // Define record
        let record = await model.findOne(options);
        if (!record || !record.id) {
          return next(new Error('404 database.delete.notFound'));
        }

        // Define result
        let result = await model.destroy({ where: { id: record.id } }) ? { id: record.id } : null;
        if (!result || !result.id) {
          return next(new Error('404 database.delete.notDeleted'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Update
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.update = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.update.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context view
        options.contextView = 'update';

        // Define context user
        options.contextUser = params.contextUser || {};
        delete params.contextUser;

        // Define where
        options.where = {
          id: params.id
        };

        for (let field in params) {
          if (!params[field]) {
            params[field] = null;
          }
        }

        // Define record
        let record = await model.update(params, options);

        // Define result
        let result = record && record[0] ? await model.findOne({ ...options, contextView: 'select', where: { id: params.id } }) : null;
        if (!result || !result.id) {
          return next(new Error('404 database.update.notFound'));
        }

        // // Verify record bulk
        if (record && record[0] && params.bulk) {
          for (let b in params.bulk) {
            let bulk = params.bulk[b];
            if (bulk && bulk.model && bulk.field && bulk.other && app.models[bulk.model]) {
              let items = (bulk.items || []).map((o) => {
                return {
                  [bulk.field]: result.id,
                  [bulk.other]: o.id
                };
              });
              await app.models[bulk.model].destroy({ where: [{ [bulk.field]: params.id }], force: true });
              await app.models[bulk.model].bulkCreate(items);
            }
          }
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Create
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.create = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params) {
          return next(new Error('400 database.create.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context view
        options.contextView = 'create';

        // Define context user
        options.contextUser = params.contextUser || {};
        delete params.contextUser;
        if (!params.id) {
          delete params.id;
        }

        for (let field in params) {
          if (!params[field]) {
            params[field] = null;
          }
        }

        // Define record
        let record = await model.create(params, options);

        // Define result
        let result = record && record.id ? await model.findOne({ ...options, contextView: 'select', where: { id: record.id } }) : null;
        if (!result || !result.id) {
          return next(new Error('404 database.create.notFound'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Download file
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.selectField = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params) {
          return next(new Error('400 database.select.invalidParameters'));
        }

        // Define file
        let file = app.config.varPath + '/files/' + params.value;

        // Define size
        let size = params.size;

        // Verify size
        if (size && ['small', 'large'].indexOf(size) > -1) {

          // Define test
          let test = file + '.' + size;

          // Verify test
          if (!app.helpers.fs.existsSync(test)) {
            let data = await app.helpers.jimp.read(file);
            data.resize(size == 'small' ? 128 : 768, app.helpers.jimp.AUTO);
            data.quality(size == 'small' ? 85 : 95);
            await data.writeAsync(test);
          }
          res.sendFile(test);
        } else {
          res.sendFile(file);
        }
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * Upload file
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.updateField = (model) => {
    return async (req, res, next) => {
      try {
        let upload = app.middleware.storage.uploadFile();
        upload(req, res, async (error) => {

          // Define parameters
          let params = req.data;

          // Verify parameters
          if (!model || !params || error) {
            return next(new Error('400 database.update.invalidParameters'));
          }

          // Define options
          let options = {};

          // Define context view
          options.contextView = 'update';

          // Define context user
          options.contextUser = params.contextUser || {};
          delete params.contextUser;

          // Define where
          options.where = {
            id: req.data.id
          };

          // Define result
          let result = {
            [req.data.field]: req.data.file ? req.data.file.filename : null
          };

          // Update record
          await model.update(result, options);

          // Return record
          res.status(200).send(result);
        });
      } catch (error) {
        next(error);
      }
    };
  };


  /**
   * process
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.process = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.process.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'process';

        // Define context user
        options.contextUser = params.contextUser || {};
        delete params.contextUser;

        // Define where
        options.where = {
          id: params.id
        };

        // Define record
        let record = await model.update({ status: 'process' }, options);

        // Define result
        let result = record && record[0] ? await model.findOne({ ...options, scope: 'select', where: { id: params.id } }) : null;
        if (!result || !result.id) {
          return next(new Error('404 database.update.notFound'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * cancel
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.cancel = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.cancel.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'cancel';

        // Define context user
        options.contextUser = params.contextUser || {};
        delete params.contextUser;

        // Define where
        options.where = {
          id: params.id
        };

        // Define record
        let record = await model.update({ status: 'cancel' }, options);

        // Define result
        let result = record && record[0] ? await model.findOne({ ...options, scope: 'select', where: { id: params.id } }) : null;
        if (!result || !result.id) {
          return next(new Error('404 database.update.notFound'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };

  /**
   * disable
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.disable = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.disable.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'disable';

        // Define context user
        options.contextUser = params.contextUser || {};
        delete params.contextUser;

        // Define where
        options.where = {
          id: params.id
        };

        // Define record
        let record = await model.update({ status: 'disable' }, options);

        // Define result
        let result = record && record[0] ? await model.findOne({ ...options, scope: 'select', where: { id: params.id } }) : null;
        if (!result || !result.id) {
          return next(new Error('404 database.update.notFound'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };


  /**
   * enable
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.enable = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.enable.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'enable';

        // Define context user
        options.contextUser = params.contextUser || {};
        delete params.contextUser;

        // Define where
        options.where = {
          id: params.id
        };

        // Define record
        let record = await model.update({ status: 'enable' }, options);

        // Define result
        let result = record && record[0] ? await model.findOne({ ...options, scope: 'select', where: { id: params.id } }) : null;
        if (!result || !result.id) {
          return next(new Error('404 database.update.notFound'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  };


  /**
   * error
   *
   * @param {Object} model - Model
   * @return {Function}
   */
  middleware.error = (model) => {
    return async (req, res, next) => {
      try {

        // Define parameters
        let params = req.data;

        // Verify parameters
        if (!model || !params || !params.id) {
          return next(new Error('400 database.error.invalidParameters'));
        }

        // Define options
        let options = {};

        // Define context scope
        options.contextView = 'error';

        // Define context user
        options.contextUser = params.contextUser || {};
        delete params.contextUser;

        // Define where
        options.where = {
          id: params.id
        };

        // Define record
        let record = await model.update({ status: 'error' }, options);

        // Define result
        let result = record && record[0] ? await model.findOne({ ...options, scope: 'select', where: { id: params.id } }) : null;
        if (!result || !result.id) {
          return next(new Error('404 database.update.notFound'));
        }

        // Return result
        res.send(result)
      } catch (error) {
        next(error);
      }
    };
  }

};
