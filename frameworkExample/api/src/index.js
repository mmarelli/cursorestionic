'use strict';

// Bootstrap
try {

  /**
   * Define console replacements
   * to format logs
   *
   * @param {Mixed} value
   * @return {void}
   */
  [{
    code: 'info',
    call: console.info.bind(console),
    color: '36m'
  }, {
    code: 'warn',
    call: console.warn.bind(console),
    color: '33m'
  }, {
    code: 'error',
    call: console.error.bind(console),
    color: '31m'
  }].forEach((item) => {
    console[item.code] = function () {
      item.call.apply(console, ['[' + new Date().toISOString() + '] [' + item.code.toUpperCase() + ']\x1b[' + item.color, ...arguments, '\x1b[0m']);
    };
  });

  /**
   * Define function
   * to format file name
   *
   * @param {String} value
   * @return {String}
   */
  const formatFileName = (value) => {
    return (value || '').replace(/\.e?js$/, '').replace(/\./g, ' ').replace(/_/g, ' ').replace(/\W+(.)/g, function (m, c) { return c.toUpperCase(); });
  };

  /**
   * Define
   *
   *
   */
  const childProcess = require('child_process');
  const crypto = require('crypto');
  const path = require('path');
  const fs = require('fs');
  const moment = require('moment');

  /**
   * Define application
   *
   * @see: doc/application
   */
  const app = {};

  // Define log
  const log = [];

  /**
   * Define config
   * in application context
   *
   * @see: doc/config
   */
  app.config = {};

  // Define config server path
  app.config.serverPath = path.join(__dirname + '/..');
  if (!fs.existsSync(app.config.serverPath)) {
    throw new Error('System path was not found');
  }

  // Define server package path
  app.config.serverPackagePath = path.join(app.config.serverPath + '/package.json');
  if (!fs.existsSync(app.config.serverPackagePath)) {
    throw new Error('Package path was not found');
  }

  // Define src path
  app.config.srcPath = path.join(app.config.serverPath + '/src');
  if (!fs.existsSync(app.config.srcPath)) {
    throw new Error('Source path was not found');
  }

  // Define var path
  app.config.varPath = path.join(app.config.serverPath + '/var');
  if (!fs.existsSync(app.config.varPath)) {
    throw new Error('Storage path was not found');
  }

  // Define paths
  let paths = ['environments', 'helpers', 'middleware', 'models', 'routes', 'tasks', 'views'];
  for (let pathName of paths) {
    app.config[pathName + 'Path'] = path.join(app.config.srcPath + '/' + pathName);
  }

  // Define from environment file
  let environment = app.config.environmentsPath + '/environment.js';
  if (fs.existsSync(environment)) {
    let items = require(environment);
    if (items) {
      for (let item in items) {
        app.config[item] = items[item];
      }
    }
  }

  /**
   * Define helpers
   * in application context
   *
   * @see: doc/helpers
   */
  app.helpers = {};
  if (fs.existsSync(app.config.helpersPath)) {
    let items = fs.readdirSync(app.config.helpersPath, {withFileTypes: true});
    for (let item of items) {
      if (item.isFile() && item.name.match(/\.js$/)) {
        app.helpers[formatFileName(item.name)] = require(app.config.helpersPath + '/' + item.name);
      }
    }
  }

  /**
   * Define middleware
   * in application context
   *
   * @see: doc/middleware
   */
  app.middleware = {};
  if (fs.existsSync(app.config.middlewarePath)) {
    let items = fs.readdirSync(app.config.middlewarePath, {withFileTypes: true});
    for (let item of items) {
      if (item.isFile() && item.name.match(/\.js$/)) {
        app.middleware[formatFileName(item.name)] = require(app.config.middlewarePath + '/' + item.name);
      }
    }
  }

  /**
   * Define models
   * in application context
   *
   * @see: doc/models
   */
  app.models = {};
  if (fs.existsSync(app.config.modelsPath)) {
    let items = fs.readdirSync(app.config.modelsPath, {withFileTypes: true});
    for (let item of items) {
      if (item.isFile() && item.name.match(/\.js$/)) {
        app.models[formatFileName(item.name)] = require(app.config.modelsPath + '/' + item.name);
      }
    }
  }

  /**
   * Define routes
   * in application context
   *
   * @see: doc/routes
   */
  app.routes = {};
  if (fs.existsSync(app.config.routesPath)) {
    let items = fs.readdirSync(app.config.routesPath, {withFileTypes: true});
    for (let item of items) {
      if (item.isFile() && item.name.match(/\.js$/)) {
        app.routes[formatFileName(item.name)] = require(app.config.routesPath + '/' + item.name);
      }
    }
  }

  /**
   * Define tasks
   * in application context
   *
   * @see: doc/tasks
   */
  app.tasks = {};
  if (fs.existsSync(app.config.tasksPath)) {
    let items = fs.readdirSync(app.config.tasksPath, {withFileTypes: true});
    for (let item of items) {
      if (item.isFile() && item.name.match(/\.js$/)) {
        app.tasks[formatFileName(item.name)] = require(app.config.tasksPath + '/' + item.name);
      }
    }
  }

  /**
   * Define views
   * in application context
   *
   * @see: doc/views
   */
  app.views = {};
  if (fs.existsSync(app.config.viewsPath)) {
    let items = fs.readdirSync(app.config.viewsPath, {withFileTypes: true});
    for (let item of items) {
      if (item.isFile() && item.name.match(/\.ejs$/)) {
        app.views[formatFileName(item.name)] = fs.readFileSync(app.config.viewsPath + '/' + item.name);
      }
    }
  }

  /**
   * Create helpers
   * in application context
   *
   * @see: doc/helpers
   */
  for (let h in app.helpers) {
    let helper = {};
    app.helpers[h](app, helper);
    app.helpers[h] = helper;
  }

  // Define helpers from node
  app.helpers.crypto = crypto;
  app.helpers.path = path;
  app.helpers.fs = fs;
  app.helpers.childProcess = childProcess;
  app.helpers.moment = moment;

  // Define helpers from dependencies
  let packageItems = require(app.config.serverPackagePath);
  for (let dependency in packageItems.dependencies) {
    app.helpers[formatFileName(dependency)] = require(dependency);
  }

  // Define DB
  app.db = app.helpers.sequelize;

  /**
   * Create middleware
   * in application context
   *
   * @see: doc/helpers
   */
  for (let h in app.middleware) {
    let middleware = {};
    app.middleware[h](app, middleware);
    app.middleware[h] = middleware;
  }

  /**
   * Create tasks
   * in application context
   *
   * @see: doc/tasks
   */
  for (let h in app.tasks) {
    let task = {};
    app.tasks[h](app, task);
    app.tasks[h] = task;
  }

  /**
   * Create models
   * in application context
   *
   * @see: doc/models
   */
  for (let m in app.models) {
    let model = {};
    app.models[m](app, model);
    app.models[m] = model;
  }

  /**
   * Define Maria
   * in application context
   *
   * @see: doc/database/maria
   */
  if (app.config.database && app.config.database.maria && app.config.database.maria.uri) {
    app.db.sequelize = new app.db(
      app.config.database.maria.uri,
      app.config.database.maria.options || {}
    );
  }

  /**
   * Define MySQL
   * in application context
   *
   * @see: doc/database/mysql
   */
  if (app.config.database && app.config.database.mysql && app.config.database.mysql.uri) {
    app.db.sequelize = new app.db(
      app.config.database.mysql.uri,
      app.config.database.mysql.options || {}
    );
  }

  /**
   * Define PgSQL
   * in application context
   *
   * @see: doc/database/pgsql
   */
  if (app.config.database && app.config.database.pgsql && app.config.database.pgsql.uri) {
    app.db.sequelize = new app.db(
      app.config.database.pgsql.uri,
      app.config.database.pgsql.options || {}
    );
  }

  /**
   * Define Redis
   * in application context
   *
   * @see: doc/database/redis
   */
  if (app.config.database && app.config.database.redis && app.config.database.redis.uri) {
    app.ms = app.config.database.redis;
  }

  /**
   * Define Redis
   * in application context
   *
   * @see: doc/redis
   */
  if (app.db && app.db.sequelize) {

    /**
     * Define models
     * in application context
     *
     * @see: doc/models
     */
    for (let m in app.models) {
      try {
        let model = app.models[m];
        let table = app.db.sequelize.define(m, model.fields, {
          tableName: model.name || m,
          paranoid: model.fields.deletedAt ? true : false,
          indexes: model.indexes
        });
        if (model.beforeLoad) {
          table.beforeLoad = model.beforeLoad;
        }
        if (model.beforeFind) {
          table.beforeFind(model.beforeFind);
        }
        if (model.afterFind) {
          table.afterFind(model.afterFind);
        }
        if (model.beforeSave) {
          table.beforeValidate(model.beforeSave);
        }
        if (model.afterSave) {
          table.afterSave(model.afterSave);
        }
        if (model.access) {
          table.access = model.access;
        }
        if (model.fields) {
          table.fields = model.fields;
        }
        app.models[m] = table;
      } catch (error) {
        console.error(m, app.models[m]);
      }
    }

    /**
     * Create models
     * in application context
     *
     * @see: doc/models
     */
    for (let m in app.models) {
      try {
        if (app.models[m].beforeLoad) {
          app.models[m].beforeLoad(app.models[m]);
        }
      } catch (error) {
        console.error(m, app.models[m]);
      }
    }
  }

  /*
   * Server
   */
  if (process.env.npm_lifecycle_event && process.env.npm_lifecycle_event == 'start') {

    /**
     * Define router
     * in application context
     *
     * @see: doc/router
     */
    app.router = app.helpers.express();

    /**
     * Define router option
     * to set the etag response header
     *
     * @see: https://expressjs.com/es/api.html
     */
    //app.router.set('etag', false);

    /**
     * Define router option
     * to determine the IP address of the client connected through proxies
     *
     * @see: https://expressjs.com/es/api.html
     */
    app.router.set('trust proxy', true);

    /**
     * Define router option
     * to set the directory for the views
     *
     * @see: https://expressjs.com/es/api.html
     */
    if (app.helpers.ejs) {
      log.push('Using "ejs" views engine');
      app.router.set('views', app.config.viewsPath + '/ejs');
      app.router.set('view cache', true);
      app.router.set('view engine', 'ejs');
    }

    /**
     * Define middleware
     * to compress response bodies
     *
     * @see: https://expressjs.com/en/rechannels/middleware/compression.html
     */
    if (app.helpers.compression) {
      log.push('Using "compression" middleware');
      app.router.use(
        app.helpers.compression()
      );
    }

    /**
     * Define middleware
     * for enable cors
     *
     * @see: https://expressjs.com/en/rechannels/middleware/cors.html
     */
    if (app.helpers.cors) {
      log.push('Using "cors" middleware');
      app.router.use(
        app.helpers.cors()
      );
    }

    /**
     * Define middleware
     * for parse cookie header and populate req.cookies
     *
     * @see: https://expressjs.com/en/rechannels/middleware/cookie-parser.html
     */
    if (app.helpers.cookieParser) {
      log.push('Using "cookieParser" middleware');
      app.router.use(
        app.helpers.cookieParser()
      );
    }

    /**
     * Define middleware
     * for parse json and populate req.body
     *
     * @see: https://expressjs.com/en/rechannels/middleware/body-parser.html
     */
    if (app.helpers.bodyParser && app.helpers.bodyParser.json) {
      log.push('Using "bodyParser.json" middleware');
      app.router.use(
        app.helpers.bodyParser.json({limit: '100mb'})
      );
    }

    /**
     * Define middleware
     * for parse url and populate req.query
     *
     * @see: https://expressjs.com/en/rechannels/middleware/body-parser.html
     */
    if (app.helpers.bodyParser && app.helpers.bodyParser.urlencoded) {
      log.push('Using "bodyParser.urlencoded" middleware');
      app.router.use(
        app.helpers.bodyParser.urlencoded({limit: '100mb', extended: true})
      );
    }

    /**
     * Define middleware
     * for override with the X-HTTP-Method header (Microsoft)
     *
     * @see: https://expressjs.com/en/rechannels/middleware/method-override.html
     */
    if (app.helpers.methodOverride) {
      log.push('Using "methodOverride.microsoft" middleware');
      app.router.use(
        app.helpers.methodOverride('X-HTTP-Method')
      );
    }

    /**
     * Define middleware
     * for override with the X-HTTP-Method-Override header (Google)
     *
     * @see: https://expressjs.com/en/rechannels/middleware/method-override.html
     */
    if (app.helpers.methodOverride) {
      log.push('Using "methodOverride.google" middleware');
      app.router.use(
        app.helpers.methodOverride('X-HTTP-Method-Override')
      );
    }

    /**
     * Define middleware
     * for override with the X-Method-Override header (IBM)
     *
     * @see: https://expressjs.com/en/rechannels/middleware/method-override.html
     */
    if (app.helpers.methodOverride) {
      log.push('Using "methodOverride.ibm" middleware');
      app.router.use(
        app.helpers.methodOverride('X-Method-Override')
      );
    }

    /**
     * Define middleware
     * for controls browser DNS prefetching
     *
     * @see: https://helmetjs.github.io/docs/dns-prefetch-control
     */
    if (app.helpers.helmet && app.helpers.helmet.dnsPrefetchControl) {
      log.push('Using "helmet.dnsPrefetchControl" middleware');
      app.router.use(
        app.helpers.helmet.dnsPrefetchControl()
      );
    }

    /**
     * Define middleware
     * to prevent clickjacking
     *
     * @see: https://helmetjs.github.io/docs/frameguard
     */
    if (app.helpers.helmet && app.helpers.helmet.frameguard) {
      log.push('Using "helmet.frameguard" middleware');
      app.router.use(
        app.helpers.helmet.frameguard()
      );
    }

    /**
     * Define middleware
     * to remove the X-Powered-By header
     *
     * @see: https://helmetjs.github.io/docs/hide-powered-by
     */
    if (app.helpers.helmet && app.helpers.helmet.hidePoweredBy) {
      log.push('Using "helmet.hidePoweredBy" middleware');
      app.router.use(
        app.helpers.helmet.hidePoweredBy()
      );
    }

    /**
     * Define middleware
     * for HTTP Strict Transport Security
     *
     * @see: https://helmetjs.github.io/docs/hsts
     */
    if (app.helpers.helmet && app.helpers.helmet.hsts) {
      log.push('Using "helmet.hsts" middleware');
      app.router.use(
        app.helpers.helmet.hsts()
      );
    }

    /**
     * Define middleware
     * sets X-Download-Options for IE8+
     *
     * @see: https://helmetjs.github.io/docs/ienoopen
     */
    if (app.helpers.helmet && app.helpers.helmet.ieNoOpen) {
      log.push('Using "helmet.ieNoOpen" middleware');
      app.router.use(
        app.helpers.helmet.ieNoOpen()
      );
    }

    /**
     * Define middleware
     * to keep clients from sniffing the MIME type
     *
     * @see: https://helmetjs.github.io/docs/dont-sniff-mimetype
     */
    if (app.helpers.helmet && app.helpers.helmet.noSniff) {
      log.push('Using "helmet.noSniff" middleware');
      app.router.use(
        app.helpers.helmet.noSniff()
      );
    }

    /**
     * Define middleware
     * for handling Adobe products’ crossdomain requests
     *
     * @see: https://helmetjs.github.io/docs/crossdomain
     */
    if (app.helpers.helmet && app.helpers.helmet.permittedCrossDomainPolicies) {
      log.push('Using "helmet.permittedCrossDomainPolicies" middleware');
      app.router.use(
        app.helpers.helmet.permittedCrossDomainPolicies()
      );
    }

    /**
     * Define middleware
     * to hide the Referer header
     *
     * @see: https://helmetjs.github.io/docs/referrer-policy
     */
    if (app.helpers.helmet && app.helpers.helmet.referrerPolicy) {
      log.push('Using "helmet.referrerPolicy" middleware');
      app.router.use(
        app.helpers.helmet.referrerPolicy()
      );
    }

    /**
     * Define middleware
     * to adds some small XSS protections
     *
     * @see: https://helmetjs.github.io/docs/xss-filter
     */
    if (app.helpers.helmet && app.helpers.helmet.xssFilter) {
      log.push('Using "helmet.xssFilter" middleware');
      app.router.use(
        app.helpers.helmet.xssFilter()
      );
    }

    /**
     * Define middleware
     * for HTTP request logger
     *
     * @see: https://expressjs.com/en/rechannels/middleware/morgan.html
     */
    if (app.helpers.morgan) {
      log.push('Using "morgan" middleware');
      app.router.use(
        app.helpers.morgan('[:date[iso]] [HTTP] :remote-addr - :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"')
      );
    }

    /**
     * Define middleware
     * for load API modules
     *
     * @see app.helpers.loadModules()
     */
    if (app.config.application && app.config.application.apiPath) {
      log.push('Using "cache" middleware');
      app.router.use((app.config.application.urlPath || '') + app.config.application.apiPath,
        (req, res, next) => {
          res.setHeader('Surrogate-Control', 'no-store');
          res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate');
          res.setHeader('Pragma', 'no-cache');
          res.setHeader('Expires', '0');
          next();
        }
      );
    }

    /**
     * Define middleware
     * for load API modules
     *
     * @see app.helpers.loadModules()
     */
    if (app.config.application && app.config.application.apiPath) {
      for (let i in app.routes) {
        let route = app.helpers.express.Router();
        let model = app.models[i] || null;
        app.routes[i](app, route, model);
        app.routes[i] = route;
        app.router.use((app.config.application.urlPath || '') + app.config.application.apiPath + '/' + i, app.routes[i]);
      }
    }

    /**
     * Define middleware
     * for cache busting
     *
     * @see app.helpers.use()
     */
    if (app.config.application) {
      log.push('Using "cache" middleware');
      app.router.use((app.config.application.urlPath || '') + '/',
        (req, res, next) => {
          req.url = req.url.replace(/\/([^\/]+)\.[0-9]+\.(css|js|bmp|cur|gif|ico|jpe?g|png|svgz?|webp|webmanifest)$/, '/$1.$2');
          next();
        }
      );
    }

    /**
     * Define middleware
     * for static path
     *
     * @see app.helpers.express.static()
     */
    if (app.config.application && app.config.viewsPath) {
      log.push('Using "cache" middleware');
      app.router.use((app.config.application.urlPath || '') + '/',
        app.helpers.express.static(app.config.viewsPath + '/public', {dotfiles: 'deny', maxAge: '1 day', etag: false})
      );
    }

    /**
     * Define middleware
     * for load app
     *
     * @see
     */
    if (app.config.application && app.config.application.appPath) {
      log.push('Using "cache" middleware');
      app.router.get((app.config.application.urlPath || '') + app.config.application.appPath + '/*',
        (req, res) => {
          res.sendFile(app.config.viewsPath + '/public' + app.config.application.appPath + '/index.html');
        }
      );
    }

    /**
     * Define middleware
     * for catch errors
     *
     * @see
     */
    log.push('Using "cache" middleware');
    app.router.use('/',
      (req, res, next) => {
        next(new Error('404 page.notfound'));
      }
    );

    /**
     * Define middleware
     * for catch errors
     *
     * @see
     */
    log.push('Using "cache" middleware');
    app.router.use('/',
      (error, req, res, next) => {

        // Respond with html page
        //if (req.accepts('html')) {
        //  return res.render('404', {url: req.url});
        //}

        // Respond with json
        //if (req.accepts('json')) {
        //  return res.send({error: 'Not found'});
        //}

        // default to plain-text. send()
        //res.type('txt').send('Not found');

        let parse = (error.message || '').match(/^([0-9]+) ([A-Za-z0-9\-\_\.]+)$/);
        if (parse) {
          let statusCode = parseInt(parse[1]) || 400;
          let statusText = parse[2];
          res.status(statusCode).send({message: statusText});
        } else {
          console.error(error);
          res.status(400).send(error);
        }
      }
    );

    /**
     * Define https
     * in application context
     *
     * openssl req -nodes -new -x509 -keyout server.key -out server.crt
     *
     * @see: doc/server
     */
    let https = {};

    // Verify SSL key
    if (fs.existsSync(app.config.environmentsPath + '/ssl/server.key')) {
      https.key = fs.readFileSync(app.config.environmentsPath + '/ssl/server.key');
    }

    // Verify SSL crt
    if (fs.existsSync(app.config.environmentsPath + '/ssl/server.crt')) {
      https.cert = fs.readFileSync(app.config.environmentsPath + '/ssl/server.crt');
    }

    // Verify SSL
    //if (https.key && https.cert) {
    //  app.config.https = https;
    //}

    /**
     * Define server
     * in application context
     *
     * @see: doc/server
     */
    app.server = require(app.config.https ? 'https' : 'http').createServer(
      app.config.https,
      app.router
    );

    /**
     * Define listen port
     * to
     *
     * @see
     */
    app.server.listen(app.config.application.serverPort);
    app.server.on('listening', () => {

      // Print server listening on port
      console.info('Listening on port ' + app.server.address().port);
      console.log('');

      // Print log
      log.forEach((val) => {
        console.info(val);
      });
      console.log('');

      // Print config items
      console.info('Config');
      Object.keys(app.config).forEach((key) => {
        console.info('Option "app.config.' + key + '"');
      });
      console.log('');

      // Print helpers items
      console.info('Helpers');
      Object.keys(app.helpers).forEach((key) => {
        console.info('Helper "app.helpers.' + key + '"');
      });
      console.log('');

      // Print middleware items
      console.info('Middleware');
      Object.keys(app.middleware).forEach((key) => {
        console.info('Middleware "app.middleware.' + key + '"');
      });
      console.log('');

      // Print models items
      console.info('Models');
      Object.keys(app.models).forEach((key) => {
        console.info('Model "app.models.' + key + '"');
      });
      console.log('');

      // Print routes items
      console.info('Routes');
      Object.keys(app.routes).forEach((key) => {
        console.info('Route "app.routes.' + key + '"');
      });
      console.log('');

      // Print models items
      console.info('Tasks');
      Object.keys(app.tasks).forEach((key) => {
        console.info('Task "app.tasks.' + key + '"');
      });
      console.log('');

      // Print models items
      console.info('Views');
      Object.keys(app.views).forEach((key) => {
        console.info('View "app.views.' + key + '"');
      });
      console.log('');

      // Watch sources
      app.helpers.nodeWatch([app.config.srcPath], {delay: 1000, filter: /\.js$/, recursive: true}, (evt, name) => {
        console.warn('File %s changed.', name);
        process.exit(0);
      });
    });
  } else {
    if (process.argv && process.argv[2] && process.argv[3] && app.tasks[process.argv[2]][process.argv[3]]) {
      app.tasks[process.argv[2]][process.argv[3]](JSON.parse(process.argv[4] || '{}'));
    }
  }
} catch (error) {
  console.error(error);
}
