'use strict';

// Define module
module.exports = (app, task) => {

  /**
   * Define task
   * to create database
   *
   * @param {Object} params - Options
   * @return {Promise}
   */
  task.createDatabase = () => {
    return new Promise(async (resolve, reject) => {
      try {

        // Define result
        let result = {};

        // Create
        result.sync = await app.db.sequelize.sync({
          force: true
        });

        // Return result
        console.info('End "createDatabase" task');
        resolve(result);
      } catch (error) {
        console.error('Error in "createDatabase" task');
        console.error(error);
        reject(error);
      }
    });
  };

  /**
   * Define task
   * to update database
   *
   * @param {Object} params - Options
   * @return {Promise}
   */
  task.updateDatabase = () => {
    return new Promise(async (resolve, reject) => {
      try {

        // Define result
        let result = {};

        // Update
        result.sync = await app.db.sequelize.sync({
          alter: true
        });

        // Return result
        resolve(result);
      } catch (error) {
        console.error('Error in "updateDatabase" task');
        console.error(error);
        reject(error);
      }
    });
  };

  // usersConfig
  task.usersConfig = () => {
    return new Promise(async (resolve, reject) => {

      let result = [];
      try {
        // Create users
        result.users = [];
        result['00'] = await app.models.users.upsert({
          id: '00000000-0000-0000-0000-100000000000',
          status: 'enable',
          type: 'root',
          firstName: 'Admin',
          lastName: 'Maxmobile',
          email: 'admin@maxmobile.com.ar',
          username: 'admin',
          password: '123456',
          language: 'es'
        });
        result['01'] = await app.models.users.upsert({
          id: '00000000-0000-0000-0000-100000000001',
          status: 'enable',
          type: 'root',
          firstName: 'Administator',
          lastName: 'Maxmobile',
          email: 'administator@maxmobile.com.ar',
          username: 'administator',
          password: '123456',
          language: 'es'
        });
        result['02'] = await app.models.users.upsert({
          id: '00000000-0000-0000-0000-100000000002',
          status: 'enable',
          type: 'root',
          firstName: 'Manager',
          lastName: 'Maxmobile',
          email: 'manager@maxmobile.com.ar',
          username: 'manager',
          password: '123456',
          language: 'es'
        });
        result['03'] = await app.models.users.upsert({
          id: '00000000-0000-0000-0000-100000000003',
          status: 'enable',
          type: 'root',
          firstName: 'Operator',
          lastName: 'Maxmobile',
          email: 'operator@maxmobile.com.ar',
          username: 'operator',
          password: '123456',
          language: 'es'
        });

        resolve(result);

      } catch (error) {
        console.error('Error in "migration.usersConfig" task');
        console.log(error);
        reject(error);
      }
    });
  }


}

