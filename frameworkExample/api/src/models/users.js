'use strict';

// Define module
module.exports = (app, model) => {

  /**
   * Define access for each database method
   */
  model.access = {
    search: ['all'],
    select: ['all'],
    create: ['all'],
    update: ['all'],
    delete: ['all'],
    selectField: ['all'],
    updateField: ['all'],
    process: ['all'],
    cancel: ['all'],
    disable: ['all'],
    enable: ['all'],
    error: ['all'],
  };

  /**
   * Define fields
   */
  model.fields = {
    id: {
      type: app.db.DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      viewReadOnly: true,
      viewHidden: true,
      viewPattern: null,
      viewType: 'text',
    },
    name: {
      type: app.db.DataTypes.STRING(100),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text',
    },
    firstName: {
      type: app.db.DataTypes.STRING(100),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text',

    },
    lastName: {
      type: app.db.DataTypes.STRING(100),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text',

    },
    image: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    phone: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'tel'

    },
    email: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'email'

    },
    address: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    unit: {
      type: app.db.DataTypes.STRING(10),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    city: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    state: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    country: {
      type: app.db.DataTypes.ENUM('us', 'ar'),
      allowNull: false,
      defaultValue: 'us',
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values'
    },
    zipCode: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    username: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    password: {
      type: app.db.DataTypes.STRING(60),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'

    },
    recovery: {
      type: app.db.DataTypes.STRING(60),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'

    },
    mobilePlatform: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'

    },
    mobileVersion: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'

    },
    mobileToken: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'

    },

    browserToken: {
      type: app.db.DataTypes.STRING(100),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'

    },

    language: {
      type: app.db.DataTypes.ENUM('es', 'en'),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values',
      defaultValue: 'es'
    },
    type: {
      type: app.db.DataTypes.ENUM('root', 'undefined', 'administrator', 'manager', 'operator'),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values',
      defaultValue: 'undefined'
    },
    balance: {
      type: app.db.DataTypes.DECIMAL(12, 2),
      allowNull: false,
      defaultValue: 0,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'amount',
    },
    status: {
      type: app.db.DataTypes.ENUM('pending', 'process', 'enable', 'disable', 'error'),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values',
      defaultValue: 'pending'
    },
    notes: {
      type: app.db.DataTypes.TEXT,
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },

    createdAt: {
      type: app.db.DataTypes.DATE,
      viewHidden: true,
      allowNull: false
    },
    updatedAt: {
      type: app.db.DataTypes.DATE,
      viewHidden: true,
      allowNull: false
    },


  };

  /**
   * Define indexes
   */
  model.indexes = [{
    name: 'name',
    fields: ['name'],
    unique: false
  }, {
    name: 'email',
    fields: ['email'],
    unique: false
  }, {
    name: 'username',
    fields: ['username'],
    unique: true
  }, {
    name: 'status',
    fields: ['status'],
    unique: false
  },
  {
    name: 'type',
    fields: ['type'],
    unique: false
  }];

  /**
   * Define hook
   * on before load
   *
   * @param {Object} result - Result
   * @return {void}
   */
  model.beforeLoad = (result) => {

  };

  /**
   * Define hook
   * on before find
   */
  model.beforeFind = (result) => {

//    result.attributes=['id','name','image','type','status'];

  };

  /**
   * Define hook
   * on before save
   *
   * @param {Object} result - Result
   * @param {Object} options - Options
   * @return {Promise}
   */
  model.beforeSave = (result, options) => {

    // Default values
    if (result.isNewRecord) {
      result.balance = 0;
    }

    // Format Values
    if (result.firstName) {
      result.firstName = result.firstName.trim().toLowerCase().replace(/(?:^|\s)\S/g, function (l) { return l.toUpperCase(); }).replace(/\s\s+/g, ' ');
    }
    if (result.lastName) {
      result.lastName = result.lastName.trim().toLowerCase().replace(/(?:^|\s)\S/g, function (l) { return l.toUpperCase(); }).replace(/\s\s+/g, ' ');
    }
    if (result.firstName && result.lastName) {
      result.name = result.lastName + ', ' + result.firstName
    }
    if (result.phone) {
      result.phone = result.phone.replace(/[^-\./0-9]/g, '');
    }
    if (result.unit) {
      result.unit = result.unit.toUpperCase().replace(/\s/g, '');
    }
    if (result.email) {
      result.email = result.email.toLowerCase().trim();
    }
    if (result.username) {
      result.username = result.username.toLowerCase().trim();
    }
    if (result.password && result.password.length < 60) {
      result.password = app.helpers.bcryptjs.hashSync(result.password);
    }
    if (result.recovery && result.recovery.length < 60) {
      result.recovery = app.helpers.bcryptjs.hashSync(result.recovery);
    }

  };

  /**
   * Define hook
   * on after save
   *
   * @param {Object} result - Result
   * @param {Object} options - Options
   * @return {Promise}
   */
  model.afterSave = (result, options) => {

    // Uplad files and images

  };

};
