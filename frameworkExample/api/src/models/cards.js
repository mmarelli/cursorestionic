'use strict';

// Define module
module.exports = (app, model) => {

  /**
   * Define access for each database method
   * 
   *  For none
   *    []
   * 
   *  For Averybody
   *    ['all']
   * 
   *  For a specific profile
   *    ['<profileName>','<profileName>','<profileName>']
   *
   *  For a non authenticated user
   *    [...,'anonymous',...]
   * 
   *  For complex context
   *  {
   *    profile: <profileName>, (inclouding all and anonymous)
   *    status: <statusValue>,
   *    type: <statusValue>,
   *    <otherField>: <otherValue>
   *  }
   * 
   */
  model.access = {
    search: ['all'],
    select: ['all'],
    create: ['all'],
    update: ['all'],
    delete: ['all'],
    selectField: [],
    updateField: [],
    process: [],
    cancel: [],
    disable: [],
    enable: [],
    error: [],
  };

  /**
   * Define fields
   */
  model.fields = {
    id: {
      type: app.db.DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: app.db.DataTypes.UUIDV4,
      viewReadOnly: true,
      viewHidden: false,
      viewPattern: null,
      viewType: 'hidden'
    },
    userId: {
      type: app.db.DataTypes.UUID,
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'relation',
      viewParent: 'users'
    },
    name: {
      type: app.db.DataTypes.STRING(100),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: '^[A-Za-z0-9]{1,}.+',
      viewType: 'text'
    },
    buyDate: {
      type: app.db.DataTypes.DATE,
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'date'
    },
    number: {
      type: app.db.DataTypes.INTEGER(8),
      allowNull: false,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'number'
    },
    balance: {
      type: app.db.DataTypes.DECIMAL(12,2),
      allowNull: false,
      defaultValue: 0,
      get() {
        return parseFloat(this.getDataValue('balance') || '0');
      },
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'amount'
    },
    image: {
      type: app.db.DataTypes.STRING(50),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'avatar'
    },
    file: {
      type: app.db.DataTypes.STRING(50),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'file'
    },
    lastBus: {
      type: app.db.DataTypes.VIRTUAL,
      get() {
        return "el ultimo viaje fue a nva cba";
      },
      set(value) {
        //throw new Error('Do not try to set the virtual value!');
      },
      viewReadOnly: true,
      viewHidden: false,
      viewPattern: null,
      viewType: 'text'
    },
    type: {
      type: app.db.DataTypes.ENUM('prepaid', 'postpaid'),
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values'
    },
    status: {
      type: app.db.DataTypes.ENUM('pending', 'enable', 'disable', 'error'),
      allowNull: false,
      defaultValue: 'pending',
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'values'
    },
    notes: {
      type: app.db.DataTypes.TEXT,
      allowNull: true,
      viewReadOnly: false,
      viewHidden: false,
      viewPattern: null,
      viewType: 'textarea'
    },
    createdAt: {
      type: app.db.DataTypes.DATE,
      allowNull: false,
      viewHidden: true
    },
    updatedAt: {
      type: app.db.DataTypes.DATE,
      allowNull: false,
      viewHidden: true
    },
  };

  /**
   * Define indexes
   */
  model.indexes = [{
    name: 'name',
    fields: ['name'],
    unique: false
  }, {
    name: 'type',
    fields: ['type'],
    unique: false
  }, {
    name: 'number',
    fields: ['number'],
    unique: true
  }, {
    name: 'status',
    fields: ['status'],
    unique: false
  }];

  /**
   * Define hook
   * on before load
   *
   * @param {Object} result - Result
   * @return {void}
   */
  model.beforeLoad = (result) => {

    // Define belongs To
    result.belongsTo(app.models.users, {as: 'user', foreignKey: 'userId'});
  
  };

  /**
   * Define hook
   * on before find
   *
   * @param {Object} result - Result
   * @return {Promise}
   */
  model.beforeFind = (result) => {

    // Verify search context view
    if (result.contextView == 'search') {
      result.attributes = ['id', 'status', 'type', 'name', 'image','balance','buyDate'];
    }

    // Verify select context view
    if (result.contextView == 'select') {
    }

    result.include = result.include || [];
    result.include.push({
      model: app.models.users,
      as: 'user',
      attributes: ['id', 'name', 'image'],
    });

  };

  /**
   * Define hook
   * on before save
   *
   * @param {Object} result - Result
   * @param {Object} options - Options
   * @return {Promise}
   */
  model.beforeSave = (result, options) => {
  

  };

  /**
   * Define hook
   * on after save
   *
   * @param {Object} result - Result
   * @param {Object} options - Options
   * @return {Promise}
   */
  model.afterSave = (result, options) => {

  };

  
};
