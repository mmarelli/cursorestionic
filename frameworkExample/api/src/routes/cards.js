'use strict';

// Define module
module.exports = (app, route, model) => {

  /**
   * Define route
   * to search records
   *
   * @see app.middleware.database.search()
   */
  route.get('/',
    app.middleware.security.auth([], model, 'search'),
    app.middleware.database.search(model)
  );

  /**
   * Define route
   * to select record
   *
   * @see app.middleware.database.select()
   */
  route.get('/:id',
    app.middleware.security.auth([], model, 'select'),
    app.middleware.database.select(model)
  );

  /**
   * Define route
   * to create record
   *
   * @see app.middleware.database.create()
   */
  route.post('/',
    app.middleware.security.auth([], model, 'create'),
    app.middleware.database.create(model)
  );

  /**
   * Define route
   * to update record
   *
   * @see app.middleware.database.update()
   */
  route.put('/:id',
    app.middleware.security.auth([], model, 'update'),
    app.middleware.database.update(model)
  );

  /**
   * Define route
   * to delete record
   *
   * @see app.middleware.database.delete()
   */
  route.delete('/:id',
    app.middleware.security.auth([], model, 'delete'),
    app.middleware.database.delete(model)
  );

  /**
   * Define route
   * to select record field
   *
   * @see app.middleware.database.selectField()
   */
  route.get('/:id/:field/:value',
    app.middleware.security.auth([], model, 'selectField'),
    app.middleware.database.selectField(model)
  );

  /**
   * Define route
   * to update record field
   *
   * @see app.middleware.database.updateField()
   */
  route.put('/:id/:field',
    app.middleware.security.auth([], model, 'updateField'),
    app.middleware.database.updateField(model)
  );

  /**
   * Define route
   * to process record field
   *
   * @see app.middleware.database.process()
   */
  route.get('/:id/process',
    app.middleware.security.auth([], model, 'process'),
    app.middleware.database.process(model)
  );

  /**
   * Define route
   * to cancel record field
   *
   * @see app.middleware.database.cancel()
   */
  route.get('/:id/cancel',
    app.middleware.security.auth([], model, 'cancel'),
    app.middleware.database.cancel(model)
  );


  /**
   * Define route
   * to disable record field
   *
   * @see app.middleware.database.disable()
   */
  route.get('/:id/disable',
    app.middleware.security.auth([], model, 'disable'),
    app.middleware.database.disable(model)
  );

  /**
   * Define route
   * to enable record field
   *
   * @see app.middleware.database.enable()
   */
  route.get('/:id/enable',
    app.middleware.security.auth([], model, 'enable'),
    app.middleware.database.enable(model)
  );

  /**
   * Define route
   * to error record field
   *
   * @see app.middleware.database.error()
   */
    route.get('/:id/error',
    app.middleware.security.auth([], model, 'error'),
    app.middleware.database.error(model)
  );



};
