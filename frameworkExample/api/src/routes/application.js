'use strict';

// Define module
module.exports = (app, route) => {

    route.get('/metadata/:model/:action',
        app.middleware.security.auth(['anonymous']),
        app.middleware.metadata.config()
    );

};
