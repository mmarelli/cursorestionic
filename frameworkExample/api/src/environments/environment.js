'use strict';

// Define module
module.exports = {

  /**
   * Define application options
   * to server instance
   *
   * @see doc/config/application
   */
  application: {

    // Define title
    title: 'Maxmobile',

    // Define description
    description: 'Rapid MVC framework',

    // Define brand
    brand: 'Maxmobile',

    // Define copyright
    copyright: 'Maxmobile © 2021',

    // Define language
    language: 'es',

    // Define country
    country: 'AR',

    // Define timezone
    timezone: '-03:00',

    // Define URL base
    urlBase: 'https://www.maxmobile.com.ar',

    // Define URL path
    urlPath: '',

    // Define APP path
    appPath: '/admin',

    // Define API path
    apiPath: '/api',

    // Define WWW logo path
    wwwLogoPath: '/img/logo.png',

    // Define WWW icon path
    wwwIconPath: '/favicon.png',

    // Define color
    backgroundColor: '#121212',

    // Define server port
    serverPort: 8080
  },

  /**
   * Define database options
   * to server instance
   *
   * @see doc/config/database
   */
  database: {

    // Define MySQL options
    mysql: {
      uri: process.env.Maxmobile_MYSQL_URI || 'mysql://root:root@localhost/maxmobileJs',
      options: {
        port: 3306,
        logging: console.log,
        logQueryParameters: true
      }
    },

    // Define Redis options
    redis: {
      uri: process.env.Maxmobile_REDIS_URI || 'redis://localhost/',
      options: {
        logging: false,
        logQueryParameters: false
      }
    }
  },

  /**
   * Define security options
   * to server instance
   *
   * @see doc/config/security
   */
  security: {

    // Define https
    https: true,

    // Define Token options
    token: {
      secret: 'fb$s18*jsjLjsT2',
      expire: 0
    },

    // Define Mailgun options
    mailgun: {
      key: ''
    },

    // Define Firebase options
    firebase: {
      key: ''
    }
  },

  /**
  * Define sandbox mode options
  * to server instance
  *
  * @see doc/config/security
  */
   tasks: {
    enable: process.env.TASKS == 'enable' ? true : false,
    userId: '00000000-0000-0000-0000-100000000000',
  },


  /**
   * Define templates
   * to server instance
   *
   * @see doc/config/templates
   */
  templates: {

    // Define signin template
    userSignin: {
      subject: '',
      title: '',
      color: '',
      image: '',
      header: {
        text: ''
      },
      footer: {
        text: ''
      },
      action: {
        text: '',
        href: ''
      }
    },

    // Define signup template
    userSignup: {
      subject: '',
      title: '',
      color: '',
      image: '',
      header: {
        text: ''
      },
      footer: {
        text: ''
      },
      action: {
        text: '',
        href: ''
      }
    },

    // Define forgot template
    userForgot: {
      subject: 'Recuperar contraseña',
      title: '',
      color: '',
      image: '',
      header: {
        text: '{name},<br> ya podés cambiar tu contraseña ingresando el código de verificación: <strong>{verify}</strong>.'
      },
      footer: {
        text: ''
      },
      action: {
        text: '',
        href: ''
      }
    },

    // Define verify template
    userVerify: {
      subject: 'Recuperar contraseña',
      title: '',
      color: '',
      image: '',
      header: {
        text: '{name},<br> ya podés cambiar tu contraseña ingresando el código de verificación: <strong>{verify}</strong>.'
      },
      footer: {
        text: ''
      },
      action: {
        text: '',
        href: ''
      }
    }
  },

};
