'use strict';

// Define module
module.exports = (app, helper) => {

  /**
   * Send email
   *
   * @param {Object} params - Parameters
   * @return {Promise}
   */
  helper.sendEmail = (params) => {
    return new Promise(async (resolve, reject) => {
      try {

        // Verify parameters
        if (!params) {
          return reject(new Error('400 invalid_parameters'));
        }

        // Define file
        let file = app.config.viewsPath + '/send.ejs';

        // Define page
        let page = {};

        // Define page from
        page.from = 'No Reply <noreply@' + app.config.mailgun.host + '>';

        // Define page to
        page.to = params.to;

        // Define page subject
        page.subject = params.subject;

        // Define page title
        page.title = params.title || app.config.homepage.title;

        // Define page color
        page.color = params.color || app.config.homepage.color;

        // Define page image
        page.image = params.image || app.config.homepage.image;

        // Define page header
        page.header = {
          text: params.header && params.header.text ? params.header.text : ''
        };

        // Define page footer
        page.footer = {
          text: params.footer && params.footer.text ? params.footer.text : ''
        };

        // Define page action
        page.action = {
          text: params.action && params.action.text ? params.action.text : '',
          href: params.action && params.action.href ? params.action.href : ''
        };

        // Define result
        app.helpers.request({
          url: 'https://api.mailgun.net/v3/' + app.config.mailgun.host + '/messages',
          form: {
            from: page.from,
            to: page.to,
            subject: page.subject,
            html: await app.helpers.ejs.renderFile(file, {page})
          },
          method: 'POST',
          headers: {
            Authorization: 'Basic ' + new Buffer('api:' + app.config.mailgun.key).toString('base64')
          }
        });

        // Return result
        resolve();
      } catch(error) {
        reject(error);
      }
    });
  };

  /**
   * Send push
   *
   * @see: https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#Notification
   * @param {Object} params - Parameters
   * @return {Promise}
   */
  helper.sendPush = (params) => {
    return new Promise(async (resolve, reject) => {
      try {

        // Verify parameters
        //if (!params || !params.to || ['android', 'ios'].indexOf(params.platform) == -1 || !params.body) {
        //  return reject(helpers.httpError(400, 'INVALID_PARAMETERS'));
        //}

        // Define payload by platform
        let payload = {
          to: params.to,
          message_id: params.id,
          data: params.data || {},
          notification: {
            'title': params.title,
            'body': params.body,
            'image': params.image,
            'sound': params.sound,
            'color': params.color,
            'icon': params.icon,
            'tag': params.tag,
            'ticker': params.ticker,
            'sticky': params.sticky ? true : false,
            'click_action': params.clickAction,
            'channel_id': params.channelId
          }
        };

        // Define push
        (new app.helpers.fcmNode(app.config.firebase.key)).send(payload, (error, result) => {
          if (error) {
            console.warn('Send push to "' + params.to + '" (' + params.platform + ') response error');
            console.warn(error);
            reject(new Error(error));
          } else {
            console.info('Send push to "' + params.to + '" (' + params.platform + ') response success');
            console.info(result);
            resolve(result);
          }
        });
      } catch(error) {
        reject(error);
      }
    });
  };

  // function to encode file data to base64 encoded string
  helper.getFileInBase64 = (file) => {
    return new Promise(async (resolve, reject) => {
      try {
        let path = app.config.varPath + '/files/' + file;
        let data = app.helpers.fs.readFileSync(path);
        let result = new Buffer.from(data).toString('base64');
        console.log(path);
        resolve(result);
      } catch(error) {
        reject(error);
      }
    });
  };


  // /**
  //  * Send file
  //  *
  //  * @param {Object} params - Parameters
  //  * @return {Function}
  //  */
  // helper.send = (params) => {
  //   return async (req, res, next) => {
  //     try {
  //       res.sendFile(app.config.varPath + '/' + req.params['0']);
  //       return;
  //
  //       // Define image
  //       let image = await app.helpers.jimp.read(app.config.varPath + '/' + req.params['0']);
  //
  //       // Define query
  //       let query = {quality: 70, width: 0, height: 0, ...req.query};
  //
  //       // Define width or height
  //       if (query.width || query.height) {
  //         //image.resize(parseInt(query.width) || app.helpers.jimp.AUTO, parseInt(query.height) || app.helpers.jimp.AUTO);
  //       }
  //
  //       // Define quality
  //       if (query.quality) {
  //         //image.quality(parseInt(query.quality));
  //         image.quality(70);
  //       }
  //
  //       // Define greyscale
  //       if (query.greyscale) {
  //         image.greyscale();
  //       }
  //
  //       // Define result
  //       let result = await image.getBufferAsync(helpers.jimp.AUTO);
  //
  //       // Return result
  //       res.setHeader('Content-Type', app.helpers.jimp.MIME_JPEG);
  //       res.status(200).send(result);
  //     } catch(error) {
  //       next(error);
  //     }
  //   };
  // };

  // /**
  //  * Watermark
  //  *
  //  * @param {Object} params - Parameters
  //  * @return {Function}
  //  */
  // helper.watermark = (params) => {
  //   // https://dev.to/muhajirdev/how-to-watermark-an-image-with-node-js-4n64
  //   //Jimp.read('raw/originalimage.png').then(tpl =>(tpl.clone().write(imgActive))).then(() => (Jimp.read(imgActive))).then(tpl=>(Jimp.read('raw/logo.png').then(logoTpl => {logoTpl.opacity(0.2);return tpl.composite(logoTpl,512,512,[Jimp.BLEND_DESTINATION_OVER]);}))).then(tpl=>(tpl.write('raw/watermark.png')))
  // };
};
