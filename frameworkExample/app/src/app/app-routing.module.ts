import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { Config } from './config'
import { HttpGuard } from './core/http.guard';

const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: '/' + (Config.route?.init || '')
},] 

for (let p in Config.pages) {
  routes.push({
    path: Config.pages[p].path,
    data: Config.pages[p],
    loadChildren: Config.pages[p].load,
    canActivate: [HttpGuard]
  });
}

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
