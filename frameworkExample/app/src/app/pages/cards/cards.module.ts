import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { CardsPageRoutingModule } from './cards-routing.module';
import { CardsPage } from './cards.page';

@NgModule({
  declarations: [
    CardsPage
  ],
  entryComponents: [
    CardsPage
  ],
  imports: [
    CoreModule,
    CardsPageRoutingModule
  ]
})
export class CardsPageModule { }
