import { Component } from '@angular/core';
import { CorePage } from '../../core/core.page';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.page.html',
  styleUrls: ['./cards.page.scss'],
})
export class CardsPage extends CorePage {

  // Define views
  public views: any = {

    // Define default view
    default: {
      entity: 'cards',
      search: {
        query: '',
        queryField: 'name',
        order: 'asc',
        orderField: 'name',
        where: {}
      },
      select: {
        getParams: () => { return this.page.params; }
      },
      fields: {
        id: {
          value: null,
          match: null
         },
         userId: {
          value: null,
          match: null
         },
         name: {
          value: null,
          match: '^[A-Za-z0-9]{1,}.+'
         },
         buyDate: {
          value: null,
          match: null
         },
         number: {
          value: null,
          match: null
         },
         balance: {
          value: '0',
          match: null
         },
         image: {
          value: null,
          match: null
         },
         file: {
          value: null,
          match: null
         },
         lastBus: {
          value: null,
          match: null
         },
         type: {
          value: null,
          match: ['prepaid','postpaid'],
          filter: true
         },
         status: {
          value: 'pending',
          match: ['pending','enable','disable','error'],
          filter: true
         },
         notes: {
          value: null,
          match: null
         },
       
      }


    }
  };
}
