import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { TemplatesPageRoutingModule } from './templates-routing.module';
import { TemplatesPage } from './templates.page';

@NgModule({
  declarations: [
    TemplatesPage
  ],
  entryComponents: [
    TemplatesPage
  ],
  imports: [
    CoreModule,
    TemplatesPageRoutingModule
  ]
})
export class TemplatesPageModule { }
