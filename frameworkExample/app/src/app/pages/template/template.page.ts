import { Component } from '@angular/core';
import { CorePage } from '../../core/core.page';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.page.html',
  styleUrls: ['./templates.page.scss'],
})
export class TemplatesPage extends CorePage {

  // Define views
  public views: any = {

    // Define default view
    default: {
      entity: 'templates',
      search: {
        query: '',
        queryField: 'name',
        order: 'asc',
        orderField: 'name',
        where: {}
      },
      select: {
        getParams: () => { return this.page.params; }
      },
      fields: {
        // Felds:  pageFields
      }


    }
  };
}
