import { Component, ElementRef, ViewChild } from '@angular/core';
import { CorePage } from '../../core/core.page';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage extends CorePage {

  // Define views
  public views: any = {

  };

  @ViewChild('graph1') private graph1: ElementRef;
  @ViewChild('graph2') private graph2: ElementRef;
  @ViewChild('graph3') private graph3: ElementRef;

  ionViewDidEnter() {
    super.ionViewDidEnter();
    if (this.page.view == 'dashboard') {
      this.initCharts();
    }
  }

  initCharts() {

    new Chart(this.graph1.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Valor 1', 'Valor 2', 'Valor 3'],
        datasets: [{
          label: '# of Votes',
          data: [8, 16, 22],
          backgroundColor: [
            'rgba(255, 0, 0, 0.2)',
            'rgba(255, 255, 0, 0.2)',
            'rgba(0, 255, 0, 0.2)'
          ],
          hoverBackgroundColor: [
            '#ff0000',
            '#ffff00',
            '#00ff00'
          ]
        }]
      }
    });

    new Chart(this.graph2.nativeElement, {
      type: 'bar',
      data: {
        labels: ['Valor 1', 'Valor 2'],
        datasets: [{
          label: '# of units',
          data: [9, 14],
          backgroundColor: [
            'rgba(17, 0, 255, 0.2)',
            'rgba(255, 0, 255, 0.2)',
          ],
          hoverBackgroundColor: [
            '#1100ff',
            '#ff00ff',
          ]
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    new Chart(this.graph3.nativeElement, {
      type: 'line',
      data: {
        labels: ['Valor 1', 'Valor 2', 'Valor 3'],
        datasets: [{
          label: 'statuses',
          data: [6, 7, 4],
          backgroundColor: [
            'rgba(255, 0, 0, 0.2)',
            'rgba(255, 255, 0, 0.2)',
            'rgba(0, 255, 0, 0.2)'
          ],
          hoverBackgroundColor: [
            '#ff0000',
            '#ffff00',
            '#00ff00'
          ]
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

}


