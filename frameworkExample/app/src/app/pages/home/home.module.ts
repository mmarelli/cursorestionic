import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { HomePageRoutingModule } from './home-routing.module';
import { HomePage } from './home.page';

@NgModule({
  declarations: [
    HomePage
  ],
  entryComponents: [
    HomePage
  ],
  imports: [
    CoreModule,
    HomePageRoutingModule
  ]
})
export class HomePageModule {}
