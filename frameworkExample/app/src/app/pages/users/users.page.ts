import { Component } from '@angular/core';
import { CorePage } from '../../core/core.page';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss']
})
export class UsersPage extends CorePage {

  section = 'main';

  // Define views
  public views: any = {

    // Define default view
    default: {
      entity: 'users',
      search: {
        query: '',
        queryField: 'name',
        order: 'asc',
        orderField: 'name',
        where: {}
      },
      select: {
        getParams: () => { return this.page.params; }
      },
      fields: {
        section: {
          value: this.section
        },
        id: {
          value: null,
          match: null
        },
        name: {
          value: null,
          match: null
        },
        firstName: {
          value: null,
          match: null
        },
        lastName: {
          value: null,
          match: null
        },
        image: {
          value: null,
          match: null
        },
        phone: {
          value: null,
          match: null
        },
        email: {
          value: null,
          match: null
        },
        address: {
          value: null,
          match: null
        },
        unit: {
          value: null,
          match: null
        },
        city: {
          value: null,
          match: null
        },
        state: {
          value: null,
          match: null
        },
        country: {
          value: 'us',
          match: ['us', 'ar'],
          filter: true
        },
        zipCode: {
          value: null,
          match: null
        },
        username: {
          value: null,
          match: null
        },
        password: {
          value: null,
          match: null
        },
        recovery: {
          value: null,
          match: null
        },
        mobilePlatform: {
          value: null,
          match: null
        },
        mobileVersion: {
          value: null,
          match: null
        },
        mobileToken: {
          value: null,
          match: null
        },
        browserToken: {
          value: null,
          match: null
        },
        language: {
          value: 'es',
          match: ['es', 'en'],
        },
        balance: {
          value: null,
          match: ['withDebt', 'withoutDebt', 'positiveBalance'],
          filter: true
        },
        type: {
          value: 'undefined',
          match: [/*'root', 'undefined', */'administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
          filter: true
        },
        status: {
          value: 'values',
          match: ['enable', 'disable'],
          filter: true
        },
        notes: {
          value: null,
          match: null
        },
      }
    },

    // Define profileChangeName view
    profileChangeName: {
      entity: 'users',
      submit: 'users/user/profile',
      fields: {
        name: {
          match: '^[A-Za-z0-9]{1,}.+',
          value: null
        }
      }
    },

    // Define profileChangePhone view
    profileChangePhone: {
      entity: 'users',
      submit: 'users/user/profile',
      fields: {
        phone: {
          match: '^[0-9]{6,}',
          value: null
        },
        verifyPassword: {
          match: '^(.){6,}',
          value: null
        }
      }
    },

    // Define profileChangeEmail view
    profileChangeEmail: {
      entity: 'users',
      submit: 'users/user/profile',
      fields: {
        email: {
          match: '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$',
          value: null
        },
        verifyPassword: {
          match: '^(.){6,}',
          value: null
        }
      }
    },

    // Define profileChangeUsername view
    profileChangeUsername: {
      entity: 'users',
      submit: 'users/user/profile',
      fields: {
        username: {
          match: '^[a-zA-Z0-9_.+-@]+$',
          value: null
        },
        verifyPassword: {
          match: '^(.){6,}',
          value: null
        }
      }
    },

    // Define profileChangePassword view
    profileChangePassword: {
      entity: 'users',
      submit: 'users/user/profile',
      fields: {
        password: {
          match: '^(.){6,}',
          value: null
        },
        verifyPassword: {
          match: '^(.){6,}',
          value: null
        }
      }
    },

    // Define signin view
    signup: {
      entity: 'users',
      submit: 'users/user/signup',
      fields: {
        name: {
          match: '^[A-Za-z0-9]{1,}.+',
          value: null
        },
        email: {
          match: '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$',
          value: null
        },
        password: {
          match: '^(.){6,}',
          value: null
        }
      }
    },

    // Define signout view
    signout: {
      entity: 'users',
      submit: {
        url: '/api/users/user/signout',
        gotoHomeOnSuccess: true
      },
      fields: {
      }
    },

    // Define signin view
    signin: {
      entity: 'users',
      submit: {
        url: '/api/users/user/signin',
        gotoHomeOnSuccess: true
      },
      fields: {
        username: {
          match: '^[a-zA-Z0-9_.+-@]+$',
          value: null
        },
        password: {
          match: '^(.){6,}',
          value: null
        }
      }
    },

    // Define forgot view
    forgot: {
      entity: 'users',
      submit: 'users/user/forgot',
      fields: {
        username: {
          match: '^[a-zA-Z0-9_.+-@]+$',
          value: null
        }
      }
    },

    // Define verify view
    verify: {
      entity: 'users',
      submit: 'users/user/verify',
      fields: {
        recovery: {
          match: '^[0-9]{6,}',
          value: null
        }
      }
    },

    companies: {
      entity: 'companies',
      forceRefresh: true,
      search: {
        query: '',
        queryField: 'name',
        order: 'asc',
        orderField: 'name',
        where: {}
      },
      select: {
        getParams: () => { return this.page.params; }
      },
      fields:
      {
        status: {
          value: 'enable',
          match: ['enable', 'disable'],
          filter: true
        },
      }
    },

    propertiesLeases: {
      entity: 'propertiesLeases',
      forceRefresh: true,
      search: {
        query: '',
        queryField: 'name',
        order: 'desc',
        orderField: 'startDate',
        where: {}
      },
      select: {
        getParams: () => { return this.page.params; }
      },
      fields:
      {
        status: {
          value: 'enable',
          match: ['pending', 'enable', 'disable', 'cencel'],
          filter: true
        },
      }
    },

    transactions: {
      entity: 'transactions',
      forceRefresh: true,
      search: {
        query: '',
        queryField: 'name',
        order: 'desc',
        orderField: 'datetime',
        where: {}
      },
      select: {
        getParams: () => { return this.page.params; }
      },
      fields: {
        type: {
          value: null,
          match: ['maintenance', 'utility', 'tax', 'association', 'spending'],
          filter: true
        },
        status: {
          value: null,
          match: ['pending', 'enable', 'disable', 'canceled'],
          filter: true
        },
      }
    },

  };

  // Grid list load event
  gridListOnLoad(view) {
    console.log('gridListOnLoad', view);

    if (this.page.view == 'owners') {
      this.views.default.search.where = { type: 'owner' };
      this.views.default.fields.type.filter = false;
    } else if (this.page.view == 'tenants') {
      this.views.default.search.where = { type: 'tenant' };
      this.views.default.fields.type.filter = false;
    } else {
      this.views.default.fields.type.filter = true;
    }

  }

  // View on change event
  viewOnChange(event) {
    console.log('viewOnChange', event);

    if (event.message == 'select' && event.result && event.result.type == 'owner') {
      this.views.transactions.search.where = { ownerUserId: event.result.id };
      this.views.companies.search.where = { ownerUserId: event.result.id };
    } else if (event.message == 'select' && event.result && event.result.type == 'tenant') {
      this.views.transactions.search.where = { tenantUserId: event.result.id };
      this.views.propertiesLeases.search.where = { tenantUserId: event.result.id };
    }
  }

  addTransaction(item) {
    if (item.type == 'owner') {
      this.gotoPage('transactions/create', { type: 'owner', ownerUserId: item.id, ownerUser: item });
    } else if (item.type == 'tenant') {
      item.propertiesLeases[0].property.propertiesLeases = item.propertiesLeases;
      item.propertiesLeases[0].property.propertiesLeases[0].tenantUser = item;
      this.gotoPage('transactions/create', { type: 'tenant', tenantUserId: item.id, propertyId: item.propertiesLeases[0].property.id, propertyLeaseId: item.propertiesLeases[0].id, property: item.propertiesLeases[0].property, propertyLease: item.propertiesLeases[0], tenantUser: item });
    }
  }

  addCompany(item) {
    this.gotoPage('companies/create', { ownerUserId: item.id, ownerUser: item });
  }

  addPropertyLease(item) {
    this.gotoPage('propertiesLeases/create', { tenantUserId: item.id, tenantUser: item });
  }

  addPropertyListing(item) {
    this.gotoPage('propertiesLeases/create', { realtorUserId: item.id, realtorUser: item });
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();
    // MAX TODO: cambiar por el evento onchange
    setTimeout(() => {
      this.views.default.fields.section.value = this.section;
    }, 10);
  }
}
