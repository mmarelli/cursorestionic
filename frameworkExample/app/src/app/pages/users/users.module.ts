import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { UsersPageRoutingModule } from './users-routing.module';
import { UsersPage } from './users.page';

@NgModule({
  declarations: [
    UsersPage
  ],
  entryComponents: [
    UsersPage
  ],
  imports: [
    CoreModule,
    UsersPageRoutingModule
  ]
})
export class UsersPageModule {}
