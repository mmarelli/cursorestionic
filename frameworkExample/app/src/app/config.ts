import { environment } from '../environments/environment';

export const Config = {

  // Define page options
  page: {

    // Define title
    title: 'Framework',

    // Define version
    version: '0.0.1',

    // Define url
    url: environment.url,

    // Define language
    language: 'es',

    // Define show version
    showVersion: true,

    enableDark: false,

    // Define show options dark mode
    showOptionsDarkMode: true
  },

  // Define maps options
  maps: {
    key: 'AIzaSyDfTaB20PeEW_SQy9dA3VuOtd9jDGxKs9E'
  },

  // Define route options
  route: {
    init: 'users/signin',
    home: 'home/dashboard',
    signIn: 'users/signin',
    forceSignIn: true
  },

  // Define pages
  pages: [

    // Define page
    {
      path: 'home/dashboard',
      name: 'home.dashboard.title',
      icon: 'grid',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/home/home.module').then(m => m.HomePageModule)
    },

    // Users
    {
      path: 'users/search',
      name: 'users.search.title',
      icon: 'people',
      menu: ["all"],
      auth: ['administrator', 'manager'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/filter',
      name: 'users.filter.title',
      icon: 'search',
      menu: [],
      auth: ['administrator', 'manager'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/select/:id',
      name: 'users.select.title',
      icon: 'search',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/create',
      name: 'users.create.title',
      icon: 'create',
      menu: [],
      auth: ['administrator'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/update/:id',
      name: 'users.update.title',
      icon: 'create',
      menu: [],
      auth: ['administrator'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/delete/:id',
      name: 'users.delete.title',
      icon: 'create',
      menu: [],
      auth: ['administrator'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/profile',
      name: 'users.profile.title',
      icon: 'person-circle',
      menu: [],
      auth: ['administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/profileChangeName',
      name: 'users.profileChangeName.title',
      icon: 'create',
      menu: [],
      auth: ['administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/profileChangePhone',
      name: 'users.profileChangePhone.title',
      icon: 'create',
      menu: [],
      auth: ['administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/profileChangeEmail',
      name: 'users.profileChangeEmail.title',
      icon: 'create',
      menu: [],
      auth: ['administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/profileChangeUsername',
      name: 'users.profileChangeUsername.title',
      icon: 'create',
      menu: [],
      auth: ['administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/profileChangePassword',
      name: 'users.profileChangePassword.title',
      icon: 'create',
      menu: [],
      auth: ['administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/signout',
      name: 'users.signout.title',
      icon: 'log-out',
      menu: [],
      auth: ['administrator', 'manager', 'assistant', 'maintenance', 'realtor', 'support', 'owner', 'tenant'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/signin',
      name: 'users.signin.title',
      icon: 'log-in',
      menu: ['anonymous'],
      auth: ['anonymous'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/signup',
      name: 'users.signup.title',
      icon: 'person-add',
      menu: [],
      auth: ['none'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },
    {
      path: 'users/forgot',
      name: 'users.forgot.title',
      icon: 'key',
      menu: ['anonymous'],
      auth: ['anonymous'],
      load: () => import('./pages/users/users.module').then(m => m.UsersPageModule)
    },


    /* cards page */
    {
      path: 'cards/search',
      name: 'cards.search.title',
      icon: 'card',
      menu: ['all'],
      auth: ['all'],
      load: () => import('./pages/cards/cards.module').then(m => m.CardsPageModule)
    },
    {
      path: 'cards/select/:id',
      name: 'cards.select.title',
      icon: 'search',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/cards/cards.module').then(m => m.CardsPageModule)
    },
    {
      path: 'cards/create',
      name: 'cards.create.title',
      icon: 'create',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/cards/cards.module').then(m => m.CardsPageModule)
    },
    {
      path: 'cards/update/:id',
      name: 'cards.update.title',
      icon: 'create',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/cards/cards.module').then(m => m.CardsPageModule)
    },
    {
      path: 'cards/delete/:id',
      name: 'cards.delete.title',
      icon: 'trash',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/cards/cards.module').then(m => m.CardsPageModule)
    },
    {
      path: 'cards/enable/:id',
      name: 'cards.enable.title',
      icon: 'checkmark',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/cards/cards.module').then(m => m.CardsPageModule)
    },
    {
      path: 'cards/disable/:id',
      name: 'cards.disable.title',
      icon: 'close',
      menu: [],
      auth: ['all'],
      load: () => import('./pages/cards/cards.module').then(m => m.CardsPageModule)
    },


  ]
}
