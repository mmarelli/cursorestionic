import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Platform } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { Push, PushOptions, PushObject } from '@ionic-native/push/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Config } from './config';
import { HttpService } from './core/http.service';
import { PageService } from './core/page.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public config: any = Config;
  public user: any;
  public menu: any = [];
  public dark: any = false;

  /**
   * Constructor
   *
   * @return {Void}
   */
  constructor(
    private router: Router,
    private title: Title,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private push: Push,
    private deeplinks: Deeplinks,
    private appVersion: AppVersion,
    private translateService: TranslateService,
    private httpService: HttpService,
    public pageService: PageService
  ) {
    this.initializeApp();
  }

  /**
   * Initialize application
   *
   * @return {Void}
   */
  initializeApp() {
    // Define translate service language
    this.translateService.setDefaultLang(this.config.page.language);
    this.translateService.use(this.config.page.language);

    // Define title
    this.title.setTitle(this.translateService.instant(this.config.page.title));

    //// Define title from route
    this.router.events.subscribe((result: any) => {
      if (result && result.constructor && result.constructor.name == 'NavigationEnd') {
        for (let p in this.config.pages) {
          if (result.url.match(new RegExp('^/' + this.config.pages[p].path.replace(/\/\:([^\/]+)/g, '/([^/]+)') + '$'))) {
            if (this.config.pages[p].name) {
              this.title.setTitle(this.translateService.instant(this.config.page.title) + ' - ' + this.translateService.instant(this.config.pages[p].name));
            } else {
              this.title.setTitle(this.translateService.instant(this.config.page.title));
            }
            return;
          }
        }
      }
    });

    // Define user and menu from auth service
    this.httpService.getUserAsObservable().subscribe((result) => {
      this.user = result || null;
      if (this.user && this.user.language) {
        this.translateService.setDefaultLang(this.user.language);
        this.translateService.use(this.user.language);
      }
      this.menu = [];
      for (let p in this.config.pages) {
        let page = this.config.pages[p];
        if (page && page.path && page.menu && (page.menu.indexOf('all') > -1 || (this.user && this.user.type == 'root' && page.menu.length > 0) || page.menu.indexOf((this.user || {}).type || 'anonymous') > -1) && this.pageService.authPage(page.path)) {
          this.menu.push(page);
        }
      }
    });

    // Define ready event
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.splashScreen.hide();
      }
      this.defineDark();
      this.defineLinkEvents();
      this.definePushEvents();
      this.defineVersion();
    });
  }

  /**
   * Toggle dark mode
   *
   * @return {Void}
   */
  toggleDark(status: any) {

    // Define dark
    this.dark = status ? true : false;

    // Define dark in document body class list
    document.body.classList.toggle('dark', this.dark);

/*
    // Define background
    let background = window.getComputedStyle(document.body).getPropertyValue('--app-color-background').trim();
    if (background) {

      // Define color in meta "theme-color"
      document.querySelector('meta[name=theme-color]').setAttribute('content', background);

      // Define color in status bar (TODO: calcular dinámicamente)
      if (this.platform.is('cordova')) {
        if (this.dark || ['#000', '#111', '#222', '#121', '#212'].indexOf(background) > -1) {
          this.statusBar.styleLightContent();
        } else {
          this.statusBar.styleDefault();
        }
        this.statusBar.backgroundColorByHexString(background);
      }
    }
*/
  }

  /**
   * Define dark mode
   *
   * @return {Void}
   */
  defineDark() {
    if (this.config.page.enableDark) {
      let theme = window.matchMedia('(prefers-color-scheme: dark)');
      theme.addListener((mediaQuery) => {
        this.toggleDark(mediaQuery.matches);
      });
      this.toggleDark(theme.matches);
    }
  }

  /**
   * Define link events
   *
   * @return {Void}
   */
  defineLinkEvents() {
    if (this.config.page.enableLinkEvents && this.platform.is('cordova')) {

      // Define deep links
      this.deeplinks.route({
        '/confirm/:id': '/money/transactions/confirm'
      }).subscribe((match) => {
        console.log(JSON.stringify(match));
        //this.pageService.gotoPage(match.$route + '/' + match.$args.id);
      }, (error) => {
        console.log(JSON.stringify(error));
      });
    }
  }

  /**
   * Define push events
   *
   * @return {Void}
   */
  definePushEvents() {
    if (this.config.page.enablePushEvents && this.platform.is('cordova')) {
      this.push.hasPermission().then(() => {

        // Define push options
        let pushOptions: PushOptions = {
          ios: {
            alert: true,
            badge: true,
            sound: false
          },
          android: {},
          browser: {
            pushServiceURL: 'http://push.api.phonegap.com/v1/push'
          }
        };

        // Define push object
        let pushObject: PushObject = this.push.init(pushOptions);

        // Listen registration event
        pushObject.on('registration').subscribe((registration: any) => {
          if (registration) {
            console.log(JSON.stringify(registration));
            //let body = {};
            //body.mobilePlatform = this.platform.is('ios') ? 'ios' : (this.platform.is('android') ? 'android' : 'other');
            //body.mobileVersion = this.config.page.version;
            //body.mobileToken = registration.registrationId;
            //body.mobileUuid = null;
          }
        });

        // Listen notification event
        pushObject.on('notification').subscribe((notification: any) => {
          if (notification) {
            console.log(JSON.stringify(notification));
            //if (notification.additionalData.foreground) {
            //  this.pageService.playAudio('notification');
            //  this.pageService.showMessage({message: notification.message, force: true});
            //}
            //if (notification.additionalData.gotoPage) {
            //  this.pageService.gotoPage(notification.additionalData.gotoPage);
            //}
          }
        });

        // Listen error event
        pushObject.on('error').subscribe((error: any) => {
          console.log(JSON.stringify(error));
        });
      }).catch((error) => {
        console.log(JSON.stringify(error));
      });
    }
  }

  /**
   * Define version
   *
   * @return {Void}
   */
  defineVersion() {
    if (this.config.page.enableVersion && this.platform.is('cordova')) {
      this.appVersion.getVersionNumber().then((version) => {
        if (version) {
          this.config.page.version = version;
        }
      }).catch((error) => {
        console.log(JSON.stringify(error));
      });
    }
  }
}
