import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { HttpService } from './http.service';
import { PageService } from './page.service';

@Injectable({
  providedIn: 'root'
})
export class HttpGuard implements CanActivate {

  /**
   * Constructor
   *
   * @params {Object} httpService - HTTP service
   * @params {Object} pageService - Page service
   * @return {Void}
   */
  constructor(
    private httpService: HttpService,
    private pageService: PageService
  ) {}

  /**
   * Can activate
   *
   * @params {Object} next - Activated route snapshot
   * @return {Promise}
   */
  canActivate(next: ActivatedRouteSnapshot): Promise<boolean> {
    return new Promise(async (resolve) => {
      let user = await this.httpService.verifyUser();
      if (next.data && next.data.auth && (next.data.auth.indexOf('all') > -1 || (user && user.type == 'root') || next.data.auth.indexOf((user || {}).type || 'anonymous') > -1)) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }
}
