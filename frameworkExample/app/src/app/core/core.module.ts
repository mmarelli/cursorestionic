import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';
import { FilterPipeModule } from 'ngx-filter-pipe';

import { FormatNumberPipe } from './pipes/format-number.pipe';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { FormatDatetimePipe } from './pipes/format-datetime.pipe';
import { FormatTimePipe } from './pipes/format-time.pipe';
import { FormatTextPipe } from './pipes/format-text.pipe';
import { FormatLinkPipe } from './pipes/format-link.pipe';
import { FormatPhonePipe } from './pipes/format-phone.pipe';
import { FormatEmailPipe } from './pipes/format-email.pipe';
import { FormatAddressPipe } from './pipes/format-address.pipe';
import { FormatPositionPipe } from './pipes/format-position.pipe';
import { FormatDistancePipe } from './pipes/format-distance.pipe';

import { ItemComponent } from './components/item/item.component';
import { ItemImageComponent } from './components/item-image/item-image.component';
import { ItemFileComponent } from './components/item-file/item-file.component';
import { ItemPositionComponent } from './components/item-position/item-position.component';
import { ItemRelationComponent } from './components/item-relation/item-relation.component';
import { ListComponent } from './components/list/list.component';
import { ViewComponent } from './components/view/view.component';

@NgModule({
  declarations: [
    FormatNumberPipe,
    FormatDatePipe,
    FormatDatetimePipe,
    FormatTimePipe,
    FormatTextPipe,
    FormatLinkPipe,
    FormatPhonePipe,
    FormatEmailPipe,
    FormatAddressPipe,
    FormatPositionPipe,
    FormatDistancePipe,
    ItemComponent,
    ItemFileComponent,
    ItemImageComponent,
    ItemPositionComponent,
    ItemRelationComponent,
    ListComponent,
    ViewComponent
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    MomentModule,
    FilterPipeModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    MomentModule,
    FilterPipeModule,
    FormatNumberPipe,
    FormatDatePipe,
    FormatDatetimePipe,
    FormatTimePipe,
    FormatTextPipe,
    FormatLinkPipe,
    FormatPhonePipe,
    FormatEmailPipe,
    FormatAddressPipe,
    FormatPositionPipe,
    FormatDistancePipe,
    ItemComponent,
    ItemFileComponent,
    ItemImageComponent,
    ItemPositionComponent,
    ItemRelationComponent,
    ListComponent,
    ViewComponent
  ],
  providers: [
  ]
})
export class CoreModule {}
