import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CorePageComponent } from '../../core.page-component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-item-file',
  templateUrl: './item-file.component.html',
  styleUrls: ['./item-file.component.scss'],
})
export class ItemFileComponent extends CorePageComponent {

  // Define output event
  @Output() onChange: any = new EventEmitter<any>();
  @Output() onLoad: any = new EventEmitter<any>();

  // Define inputs
  @Input() root: any;
  @Input() edit: any;
  @Input() view: any;
  @Input() item: any;
  @Input() name: any;
  @Input() form: any;
  @Input() type: any;
  @Input() required: any;
  @Input() label: any;

  @Input() slot: string = 'start';

  // Define values
  public value: string = null;

  // Define camera
  private camera: Camera = new Camera();

  /**
   * Angular OnChanges event
   *
   * @return {Void}
   */
  ngOnChanges() {

    // Verify edit input
    this.edit = this.parseBoolean(this.edit?.toString() || this.root?.edit?.toString() || false);

    // Verify type required
    this.required = this.parseBoolean(this.required?.toString() || false);

    // Verify view input
    this.view = this.view || this.root?.view || {};

    // Verify item input
    this.item = this.item || this.root?.item || {};

    // Verify form input
    this.form = this.form || this.root?.form || {};

    // Verify name input
    this.name = this.name || null;

    // Verify type input
    this.type = this.type || null;

    // Verify label input
    this.label = this.parseBoolean(this.label?.toString() || true);

    // Define value
    if (this.item[this.name]) {
      this.value = this.item[this.name];
    }

    // Define value from form
    if (this.form && this.form.get && this.form.get(this.name)) {
      this.value = this.form.get(this.name).value;
    }

    // Emit Onload
    this.onLoad.emit(
      this
    );

  }

  /**
   * Upload
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  async upload(params: any = {}) {
    this.attach('photo');
  }

  /**
   * Remove
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  async remove(params: any = {}) {
    let actionSheet = await this.pageService.actionSheetController.create({
      header: this.pageService.translateService.instant('page.showFileRemove.title'),
      buttons: [{
        text: this.pageService.translateService.instant('page.showFileRemove.submit'),
        icon: 'trash',
        role: 'submit',
        handler: () => this.update(null)
      }, {
        text: this.pageService.translateService.instant('page.showFileRemove.cancel'),
        icon: 'close',
        role: 'cancel'
      }]
    });
    await actionSheet.present();
  }

  /**
   * Attach
   *
   * @param {String} type
   * @return {Promise}
   */
  attach(type: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (!this.pageService.platform.is('cordova')) {
        let element = document.createElement('input');
        element.type = 'file';
        element.accept = 'application/pdf';
        element.onchange = () => {
          this.update((element.files && element.files[0] ? element.files[0] : '')).then((result) => {
            resolve(result);
          }).catch((error) => {
            reject(error);
          });
        };
        element.click();
      } else {
        // todo file upload
      }
    });
  }

  /**
   * Update
   *
   * @param {Object} params - Parameters
   * @return {Promise}
   */
  update(file: any, params: any = {}): Promise<any> {
    return new Promise(async (resolve, reject) => {

      // Define event
      this.onChange.emit({
        message: 'update',
        result: file
      });

      // Verify data
      if (!this.view || !this.view.entity || !this.item || !this.item.id || !this.name) {
        return resolve(file);
      }

      // Define body
      let body = new FormData();
      body.append('file', file);

      // Define page show loading
      this.pageService.showLoading({
        message: params.loadingMessage || 'page.showLoadingOnUpdate.message',
        delay: 0
      });

      // Update body
      this.httpService.customRequest({
        method: 'PUT',
        uri: '/api/' + this.view.entity + '/' + this.item.id + '/' + this.name,
        body: body,
        headers: {
          'x-content-type': 'on'
        }
      }).then((result) => {

        this.item[this.name] = result[this.name];
        this.value = this.item[this.name];

        // Define page show success
        this.pageService.showSuccess({
          message: params.successMessage || 'page.showSuccessOnUpdate.message',
          result: result
        });

        // Define event
        this.onChange.emit({
          message: 'update',
          result: result
        });

        resolve(result);
      }).catch((error) => {

        // Define page show warning
        this.pageService.showWarning({
          message: params.warningMessage || 'page.showWarningOnUpdate.message',
          error: error
        });

        // Define event
        this.onChange.emit({
          message: 'update',
          error: error
        });

        reject(error);
      });
    });
  }

  /**
   * Format URL
   *
   * @param {Object} params - Parameters
   * @return {Promise}
   */
  makeFileUrl(params: any = {}): string {
    return this.httpService.customUrl({
      uri: '/api/' + this.view.entity + '/' + this.item.id + '/' + this.name + '/' + this.value,
      qs: {
        type: this.type || null
      }
    });
  }

  parseBoolean(value) {
    if (value == 'false') {
      return false;
    } else if (value == 'true') {
      return true;
    } else {
      return value;
    }
  }

}
