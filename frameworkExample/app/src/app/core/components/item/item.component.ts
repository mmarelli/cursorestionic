import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CorePageComponent } from '../../core.page-component';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent extends CorePageComponent {

  // Define output event
  @Output() onChange: any = new EventEmitter<any>();
  @Output() onLoad: any = new EventEmitter<any>();

  // Define inputs
  @Input() root: any;
  @Input() edit: any;
  @Input() view: any;
  @Input() form: any;
  @Input() item: any;
  @Input() name: any;
  @Input() type: any;
  @Input() required: any;
  @Input() color: any;
  @Input() value: any;
  @Input() multiple: any;
  @Input() label: any;

  /**
   * Angular OnChanges event
   *
   * @return {Void}
   */
  ngOnChanges() {

    // Verify edit input
    this.edit = this.parseBoolean(this.edit?.toString() || this.root?.edit?.toString() || false);

    // Verify type required
    this.required = this.parseBoolean(this.required?.toString() || false);

    // Verify view input
    this.view = this.view || this.root?.view || {};

    // Verify item input
    this.item = this.item || this.root?.item || {};

    // Verify form input
    this.form = this.form || this.root?.form || {};

    // Verify name input
    this.name = this.name || null;

    // Verify type input
    this.type = this.type || null;

    // Verify color input
    this.color = this.color || null;

    // Verify value input
    this.value = this.value || null;

    // Verify multiple input
    this.multiple = this.parseBoolean(this.multiple?.toString() || false);

    // Verify label input
    this.label = this.parseBoolean(this.label?.toString() || true);

    // Define value from item
    if (this.item[this.name]) {
      this.value = this.item[this.name];
    }

    // Define value from form
    if (this.form && this.form.get && this.form.get(this.name)) {
      this.value = this.form.get(this.name).value;
    }

    // Emit Onload
    this.onLoad.emit(
      this
    );

  }

  // Emit OnChange
  emmitOnChange(event) {
    this.onChange.emit(
      event
    );
  }

  parseBoolean(value) {
    if (value == 'false') {
      return false;
    } else if (value == 'true') {
      return true;
    } else {
      return value;
    }
  }

  getName(list, id) {
    let result = '';
    if(list) {
      list.forEach(element => {
        if (element.id == id) {
          result = element.name;
          return;
        }
      });
    }
    return result;
  }
}
