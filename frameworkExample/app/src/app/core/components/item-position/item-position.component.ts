import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { CorePageComponent } from '../../core.page-component';
import { Config } from '../../../config'
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-item-position',
  templateUrl: './item-position.component.html',
  styleUrls: ['./item-position.component.scss'],
})
export class ItemPositionComponent extends CorePageComponent {

  // Define output event
  @Output() onChange: any = new EventEmitter<any>();
  @Output() onLoad: any = new EventEmitter<any>();

  // Define inputs
  @Input() root: any;
  @Input() edit: any;
  @Input() view: any;
  @Input() item: any;
  @Input() form: any;
  @Input() name: any;
  @Input() type: any;
  @Input() zoom: number = 14;
  @Input() required: any;
  @Input() label: any;

  @Input() position: any;
  @Input() address1: string = null;
  @Input() address2: string = null;
  @Input() value: any;

  // Define child element
  @ViewChild('mapElement') mapElement: ElementRef;

  // Define modal
  public modal: any = null;
  public config: any = Config;

  // Define geolocation
  public geolocation = new Geolocation();
  public map: any;
  public mar: any;
  public geocoder: any;

  /**
   * Angular OnInit event
   *
   * @return {Void}
   */
  ngOnInit() {
    if (this.modal) {
      this.loadModal();
    }
  }

  /**
   * Angular OnChanges event
   *
   * @return {Void}
   */
  ngOnChanges() {

    // Verify edit input
    this.edit = this.parseBoolean(this.edit?.toString() || this.root?.edit?.toString() || false);

    // Verify type required
    this.required = this.parseBoolean(this.required?.toString() || false);

    // Verify edit input
    this.edit = this.edit || this.root?.edit || false;

    // Verify view input
    this.view = this.view || this.root?.view || {};

    // Verify item input
    this.item = this.item || this.root?.item || {};

    // Verify form input
    this.form = this.form || this.root?.form || {};

    // Verify name input
    this.name = this.name || null;

    // Verify type input
    this.type = this.type || null;

    // Verify label input
    this.label = this.parseBoolean(this.label?.toString() || true);

    // Define value from form
    if (this.form && this.form.get && this.form.get(this.name)) {
      this.value = this.form.get(this.name).value;
    }

    // Emit Onload
    this.onLoad.emit(
      this
    );
  }

  loadModal() {
    this.loadGoogleMapsLibrary().then((google: any) => {

      // Define geocoder
      this.geocoder = new google.maps.Geocoder();

      // Define position
      this.position = this.makePosition(this.position);

      // Define position
      if (this.position) {
        this.findCurrentAddress(this.position, false);
      } else {
        this.loadCurrentPosition();
      }

      // Define map
      this.map = new google.maps.Map(this.mapElement.nativeElement);
      this.map.setCenter(this.position);
      this.map.setZoom(this.zoom);

      // Define map marker
      this.mar = new google.maps.Marker({
        map: this.map,
        position: this.position,
        draggable: true //this.pageService.authPage(this.page.code + '/update/:id')
      });

      // Define map marker listeners
      // if (this.edit) {
      //   google.maps.event.addListener(this.mar, 'dragend', () => {
      //     this.findCurrentAddress({
      //       lat: this.mar.position.lat(),
      //       lng: this.mar.position.lng()
      //     }, true);
      //   });
      // }

      // Resize
      setTimeout(() => {
        google.maps.event.trigger(this.map, 'resize');
      }, 100);
    }).catch((error) => {
      console.log(error);
    });
  }

  makePosition(position) {
    if (position && position.type == 'Point' && position.coordinates && position.coordinates.length == 2) {
      return {
        lat: position.coordinates[0],
        lng: position.coordinates[1]
      };
    }
    if (position) {
      return {
        lat: position.lat || position.latitude,
        lng: position.lng || position.longitude
      };
    }
    return null;
  }

  loadGoogleMapsLibrary(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (window['google']) {
        return resolve(window['google']);
      }
      let element = document.createElement('script');
      element.id = 'google-maps-api-script';
      element.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.config.maps.key + '&libraries=places';
      element.type = 'text/javascript';
      element.onload = () => {
        resolve(window['google']);
      };
      element.onerror = () => {
        reject();
      }
      document.body.appendChild(element);
    });
  }

  loadCurrentPosition() {
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true, timeout: 30000 }).then((result) => {
      if (result && result.coords) {
        this.position = {
          lat: result.coords.latitude,
          lng: result.coords.longitude
        };
        this.mar.setPosition(this.position);
        this.map.setCenter(this.position);
        this.findCurrentAddress(this.position, true);
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  findCurrentPosition(data) {
    this.geocoder.geocode({ 'address': data.target.value }, (results, status) => {
      if (status === 'OK' && results[0] && results[0].geometry && results[0].geometry.location) {
        this.position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };
        this.mar.setPosition(this.position);
        this.map.setCenter(this.position);
        this.findCurrentAddress(this.position, false);
      }
    });
  }

  findCurrentAddress(location, updateAddressUser) {
    this.geocoder.geocode({ location: location }, (result) => {
      this.address1 = this.address1 || null;
      this.address2 = null;
      if (result && result[0] && result[0].formatted_address) {
        this.address2 = (result[0].formatted_address || '').trim();
      }
      if (this.address2 && (updateAddressUser || !this.address1)) {
        this.address1 = this.address2;
      }
    });
  }

  //  /*
  //   * Position methods
  //   */
  //
  //  getCurrentPosition(params: any = {}): Promise<any> {
  //    return new Promise((resolve, reject) => {
  //      //this.geolocation.getCurrentPosition(params).then((result) => {
  //      //  if (result && result.coords) {
  //      //    resolve({
  //      //      lat: result.coords.latitude,
  //      //      lng: result.coords.longitude
  //      //    });
  //      //  } else {
  //      //    reject();
  //      //  }
  //      //}).catch(reject);
  //    });
  //  }

  /**
   * Show modal
   *
   * @param {Object} params - Parameters
   * @return {Promise}
   */
  async showModal(params: any = {}) {

    // Create modal
    let modal = await this.pageService.modalController.create({
      component: ItemPositionComponent,
      componentProps: params
    });

    // Define modal on did dismiss
    modal.onDidDismiss().then((result) => {
      if (result && result.data) {
        this.update(result.data);
      }
    }).catch((error) => {
    });

    // Define modal present
    await modal.present();
  }

  /**
   * Hide modal update
   *
   * @return {Void}
   */
  hideModalUpdate() {
    this.modal.dismiss({
      position: {
        type: 'Point',
        coordinates: [this.mar.position.lat(), this.mar.position.lng()]
      },
      address1: this.address1,
      address2: this.address2
    });
  }

  /**
   * Hide modal cancel
   *
   * @return {Void}
   */
  hideModalCancel() {
    this.modal.dismiss();
  }

  /**
   * Update
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  update(data: any = {}) {

    // Define event
    this.onChange.emit({
      message: 'update',
      result: data
    });

    // Define item
    this.view.item = { ...this.view.item, ...data };
  }

  parseBoolean(value) {
    if (value == 'false') {
      return false;
    } else if (value == 'true') {
      return true;
    } else {
      return value;
    }
  }

}
