import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CorePageComponent } from '../../core.page-component';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent extends CorePageComponent {

  // Define output event
  @Output() onChange: any = new EventEmitter<any>();
  @Output() onLoad: any = new EventEmitter<any>();

  // Define inputs
  @Input() root: any;
  @Input() edit: any;
  @Input() page: any;
  @Input() view: any;
  @Input() item: any;
  @Input() form: any;

  // Define page path
  pagePath = this.pageService.getCurrentPage().path;

  // Define page service subscription
  pageServiceSubscription = this.pageService.pageEventListener.subscribe((data: any) => {
    if (data && data.event == 'refresh' && (data.path == this.pagePath || data.force)) {
      this.select();
    }
  });

  /**
   *  Emit Onload
   */
  ngOnInit() {
    this.onLoad.emit(
      this
    );
  }

  /**
   *  Page Event Un subscription
   */
  ngOnDestroy() {
    this.pageServiceSubscription.unsubscribe();
  }

  /**
   * Angular OnChanges event
   *
   * @return {Void}
   */
  ngOnChanges() {

    // Verify edit input
    this.edit = this.parseBoolean(this.edit?.toString() || this.root?.edit?.toString() || false);

    // Verify page input
    this.page = this.page || this.root?.page || {};

    // Verify view input
    this.view = this.view || this.root?.view || {};

    // Verify item input
    this.item = this.item || {};

    // Verify view params
    if (this.view.select && this.view.select.getParams) {
      let params = this.view.select.getParams();
      this.item = { ...this.item, ...this.view.select.getParams() };
    }

    // Verify form input
    this.form = this.form || this.pageService.makeForm(this.view.fields || {}, this.item);

    // Verify if refresh is needed
    if (this.view && this.view.forceRefresh) {
      this.select();
    }
  }

  /**
   * Select
   *
   * @param {Object} params - Parameters
   * @param {String} params.url - Url
   * @param {String} params.loadingMessage - Loading message
   * @param {String} params.successMessage - Success message
   * @param {String} params.warningMessage - Warning message
   * @return {Void}
   */
  select() {
    console.log(this.pagePath + ' ' + (this.view ? this.view.entity : '') + 'View.select()')

    let params;
    
    if (this.view.select && this.view.select.getParams) {
      params = this.view.select.getParams();
    }

    if (!this.view || !this.view.entity || !params || !params.id) {
      return;
    }
    
    // Define form
    this.form = null;

    // Define item
    this.item = null;

    // Define url
    params.url = params.url || '/api/' + this.view.entity + '/' + params.id;

    // Define page show loading
    this.pageService.showLoading({
      message: params.loadingMessage || 'page.showLoadingOnSelect.message',
      delay: 1000
    });

    // Define http get
    this.httpService.customGet(params.url, params.query).then((result) => {

      // Define form
      this.form = this.pageService.makeForm(this.view.fields || {}, result || {});

      // Define item
      this.item = result || {};

      // Define page show success
      this.pageService.showSuccess({
        message: params.successMessage || 'page.showSuccessOnSelect.message',
        result: result
      });

      // Define event
      this.onChange.emit({
        message: 'select',
        result: result
      });
    }).catch((error) => {

      // Define page show warning
      this.pageService.showWarning({
        message: params.warningMessage || 'page.showWarningOnSelect.message',
        error: error
      });

      // Define event
      this.onChange.emit({
        message: 'select',
        error: error
      });
    });
  }

  /**
   * Create
   *
   * @param {Object} params - Parameters
   * @param {String} params.url - Url
   * @param {String} params.loadingMessage - Loading message
   * @param {String} params.successMessage - Success message
   * @param {String} params.warningMessage - Warning message
   * @return {Void}
   */
  create(params: any = {}) {
    console.log(this.pagePath + ' ' + (this.view ? this.view.entity : '') + 'View.create()')

    // Define url
    params.url = params.url || '/api/' + this.view.entity + '/';

    // Define page show loading
    this.pageService.showLoading({
      message: params.loadingMessage || 'page.showLoadingOnCreate.message',
      delay: 0
    });

    // TODO MAX: ver esto
    if (!this.form.value.id) {
      delete this.form.value.id;
    }

    // Define http post
    this.httpService.customPost(params.url, params.body || this.form.value).then((result) => {

      // Define page goto
      if (params.gotoPageOnSuccess && typeof params.gotoPageOnSuccess == 'function') {
        params.gotoPageOnSuccess(result);
      } else if (params.gotoPageOnSuccess) {
        this.gotoPage(params.gotoPageOnSuccess, result, { replaceUrl: true });
      } else if (params.gotoHomeOnSuccess) {
        this.gotoHome();
      } else if (params.gotoBackOnSuccess) {
        this.gotoBack();
      } else {
        this.gotoPage(this.view.entity + '/select/:id', { id: result.id }, { replaceUrl: true });
      }

      // Define page show success
      this.pageService.showSuccess({
        message: params.successMessage || 'page.showSuccessOnCreate.message',
        result: result
      });

      // Define event
      this.onChange.emit({
        message: 'create',
        result: result
      });

      // Fire event
      this.pageService.pageEvent.next({
        action: 'create',
        entity: this.view.entity,
        params: params,
        result: result
      })

    }).catch((error) => {

      // Define page show warning
      this.pageService.showWarning({
        message: params.warningMessage || 'page.showWarningOnCreate.message',
        error: error
      });

      // Define event
      this.onChange.emit({
        message: 'create',
        error: error
      });
    });
  }

  /**
   * Update
   *
   * @param {Object} params - Parameters
   * @param {String} params.url - Url
   * @param {String} params.loadingMessage - Loading message
   * @param {String} params.successMessage - Success message
   * @param {String} params.warningMessage - Warning message
   * @return {Void}
   */
  update(params: any = {}) {
    console.log(this.pagePath + ' ' + (this.view ? this.view.entity : '') + 'View.update()')

    // Define url
    params.url = params.url || '/api/' + this.view.entity + '/' + this.form.value.id;

    // Define page show loading
    this.pageService.showLoading({
      message: params.loadingMessage || 'page.showLoadingOnUpdate.message',
      delay: 0
    });

    // Define http put
    this.httpService.customPut(params.url, params.body || this.form.value).then((result) => {

      // Define page goto
      if (params.gotoPageOnSuccess && typeof params.gotoPageOnSuccess == 'function') {
        params.gotoPageOnSuccess(result);
      } else if (params.gotoPageOnSuccess) {
        this.gotoPage(params.gotoPageOnSuccess, result, { replaceUrl: true });
      } else if (params.gotoHomeOnSuccess) {
        this.gotoHome();
      } else if (params.gotoBackOnSuccess) {
        this.gotoBack();
      } else {
        this.gotoPage(this.view.entity + '/select/:id', { id: result.id }, { replaceUrl: true });
      }

      // Define page show success
      this.pageService.showSuccess({
        message: params.successMessage || 'page.showSuccessOnUpdate.message',
        result: result
      });

      // Define event
      this.onChange.emit({
        message: 'update',
        result: result
      });

      // Fire event
      this.pageService.pageEvent.next({
        action: 'update',
        entity: this.view.entity,
        params: params,
        result: result
      })

    }).catch((error) => {

      // Define page show warning
      this.pageService.showWarning({
        message: params.warningMessage || 'page.showWarningOnUpdate.message',
        error: error
      });

      // Define event
      this.onChange.emit({
        message: 'update',
        error: error
      });
    });
  }

  /**
   * Delete
   *
   * @param {Object} params - Parameters
   * @param {String} params.url - Url
   * @param {String} params.loadingMessage - Loading message
   * @param {String} params.successMessage - Success message
   * @param {String} params.warningMessage - Warning message
   * @return {Void}
   */
  delete(params: any = {}) {
    console.log(this.pagePath + ' ' + (this.view ? this.view.entity : '') + 'View.delete()')

    // Define url
    params.url = params.url || '/api/' + this.view.entity + '/' + this.form.value.id;

    // Define page show loading
    this.pageService.showLoading({
      message: params.loadingMessage || 'page.showLoadingOnDelete.message',
      delay: 0
    });

    // Define http delete
    this.httpService.customDelete(params.url).then((result) => {

      // Define page goto
      if (params.gotoPageOnSuccess && typeof params.gotoPageOnSuccess == 'function') {
        params.gotoPageOnSuccess(result);
      } else if (params.gotoPageOnSuccess) {
        this.gotoPage(params.gotoPageOnSuccess, result, { replaceUrl: true });
      } else if (params.gotoHomeOnSuccess) {
        this.gotoHome();
      } else if (params.gotoBackOnSuccess) {
        this.gotoBack();
      } else {
        this.gotoBack();
      }

      // Define page show success
      this.pageService.showSuccess({
        message: params.successMessage || 'page.showSuccessOnDelete.message',
        result: result
      });

      // Define event
      this.onChange.emit({
        message: 'delete',
        result: result
      });

      // Fire event
      this.pageService.pageEvent.next({
        action: 'delete',
        entity: this.view.entity,
        params: params,
        result: result
      })

    }).catch((error) => {

      // Define page show warning
      this.pageService.showWarning({
        message: params.warningMessage || 'page.showWarningOnDelete.message',
        error: error
      });

      // Define event
      this.onChange.emit({
        message: 'delete',
        error: error
      });
    });
  }

  /**
   * Submit
   *
   * @param {Object} params - Parameters
   * @param {String} params.url - Url
   * @param {String} params.mode - (create / update)
   * @param {String} params.loadingMessage - Loading message
   * @param {String} params.successMessage - Success message
   * @param {String} params.warningMessage - Warning message
   * @return {Void}
   */
  submit(params: any = {}) {
    console.log(this.pagePath + ' ' + (this.view ? this.view.entity : '') + 'View.submit()')

    // Verify url
    if (!params.url && this.view.submit) {
      params = this.view.submit;
    }

    // Verify url
    if (!params.url && params.mode) {
      if (params.mode == 'update') {
        return this.update(params);
      } else {
        return this.create(params);
      }
    }

    // Verify url
    if (!params.url) {
      if (this.form.value && this.form.value.id) {
        return this.update(params);
      } else {
        return this.create(params);
      }
    }

    // Define page show loading
    this.pageService.showLoading({
      message: params.loadingMessage || 'page.showLoadingOnSubmit.message',
      delay: 0
    });

    // Define http post
    this.httpService.customPost(params.url, params.body || this.form.value).then((result) => {

      // Define page goto
      if (params.gotoPageOnSuccess && typeof params.gotoPageOnSuccess == 'function') {
        params.gotoPageOnSuccess(result);
      } else if (params.gotoPageOnSuccess) {
        this.gotoPage(params.gotoPageOnSuccess, result, { replaceUrl: true });
      } else if (params.gotoHomeOnSuccess) {
        this.gotoHome();
      } else if (params.gotoBackOnSuccess) {
        this.gotoBack();
      } else {
        this.gotoBack();
      }

      // Define page show success
      this.pageService.showSuccess({
        message: params.successMessage || 'page.showSuccessOnSubmit.message',
        result: result
      });

      // Define event
      this.onChange.emit({
        message: 'submit',
        result: result
      });
    }).catch((error) => {
      console.error(error);

      // Define page show warning
      this.pageService.showWarning({
        message: params.warningMessage || 'page.showWarningOnSubmit.message',
        error: error
      });

      // Define event
      this.onChange.emit({
        message: 'submit',
        error: error
      });
    });
  }

  /**
   * Cancel
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  cancel(params: any = {}) {
    console.log(this.pagePath + ' ' + (this.view ? this.view.entity : '') + 'View.cancel()')

    // Define page goto
    this.pageService.gotoBack();

    // Define event
    this.onChange.emit({
      message: 'cancel'
    });
  }

  /**
   * Remove
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  async remove(params: any = {}) {
    // <ion-text color="danger" [innerHTML]="view.page.code + '.delete.legend' | translate:view.item"></ion-text>

    let actionSheet = await this.pageService.actionSheetController.create({
      header: this.pageService.translateService.instant(params.message || 'page.showRemove.title'),
      buttons: [{
        text: this.pageService.translateService.instant('page.showRemove.submit'),
        icon: 'trash',
        role: 'submit',
        handler: () => this.delete(params)
      }, {
        text: this.pageService.translateService.instant('page.showRemove.cancel'),
        icon: 'close',
        role: 'cancel'
      }]
    });

    await actionSheet.present();
  }

  parseBoolean(value) {
    if (value == 'false') {
      return false;
    } else if (value == 'true') {
      return true;
    } else {
      return value;
    }
  }

}
