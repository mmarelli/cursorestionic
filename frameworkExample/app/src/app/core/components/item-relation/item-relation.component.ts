import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CorePageComponent } from '../../core.page-component';

@Component({
  selector: 'app-item-relation',
  templateUrl: './item-relation.component.html',
  styleUrls: ['./item-relation.component.scss'],
})
export class ItemRelationComponent extends CorePageComponent {

  // Define output event
  @Output() onChange: any = new EventEmitter<any>();
  @Output() onLoad: any = new EventEmitter<any>();

  // Define inputs
  @Input() root: any;
  @Input() edit: any;
  @Input() view: any;
  @Input() item: any;
  @Input() name: any;
  @Input() type: any;
  @Input() color: any;
  @Input() required: any;
  @Input() label: any;
  @Input() filter: any;

  @Input() list: any;
  @Input() from: string;

  @Input() iconName: string = 'image';
  @Input() iconSize: string = 'small';
  @Input() iconType: string = null;

  @Input() max: number = 0;
  @Input() min: number = 0;

  // Define modal
  public modal: any = null;

  /**
   * Angular OnChanges event
   *
   * @return {Void}
   */
  ngOnChanges() {

    // Verify edit input
    this.edit = this.parseBoolean(this.edit?.toString() || this.root?.edit?.toString() || false);

    // Verify type required
    this.required = this.parseBoolean(this.required?.toString() || false);

    // Verify view input
    this.view = this.view || this.root?.view || {};

    // Verify item input
    this.item = this.item || this.root?.item || {};

    // Verify name input
    this.name = this.name || null;

    // Verify type input
    this.type = this.type || null;

    // Verify type input
    this.filter = this.filter || null;

    // Verify from input
    this.from = this.from || this.name;

    // Verify label input
    this.label = this.parseBoolean(this.label?.toString() || true);

    // Verify list input
    // TODO MAX: ver que pasa cuando viene el valor de list y en que situacion 
    //    if (!this.list || !this.list.length) {
    let index = (this.name || this.from).replace(/Id$/, '');
    if (index && this.item[index]) {
      this.update(Array.isArray(this.item[index]) ? this.item[index] : [this.item[index]]);
    } else {
      this.update([]);
    }
    //  }

    // TODO MAX: parche para setear el valor q viene por parametro
    //this.update(this.list);

    // Emit Onload
    this.onLoad.emit(
      this
    );

  }

  objectsEqual(o1, o2) {
    return Object.keys(o1).length === Object.keys(o2).length
      && Object.keys(o1).every(p => o1[p] === o2[p]);
  }

  /**
   * Show modal
   *
   * @return {Void}
   */
  async showModal() {

    // Create modal
    let modal = await this.pageService.modalController.create({
      component: ItemRelationComponent,
      componentProps: {
        root: null,
        edit: null,
        view: this.view,
        item: this.item,
        name: this.name,
        type: this.type,
        list: this.list,
        from: this.from,
        filter: this.filter,
        iconName: this.iconName,
        iconSize: this.iconSize,
        iconType: this.iconType,
        max: this.max,
        min: this.min
      }
    });

    // Define modal on did dismiss
    modal.onDidDismiss().then((result) => {
      if (result && result.data) {
        this.update(result.data);
      }
    }).catch((error) => {
    });

    // Define modal present
    await modal.present();
  }

  /**
   * Hide modal update
   *
   * @return {Void}
   */
  hideModalUpdate() {
    this.modal.dismiss(this.list);
  }

  /**
   * Hide modal cancel
   *
   * @return {Void}
   */
  hideModalCancel() {
    this.modal.dismiss();
  }

  /**
   * Select item
   *
   * @param {Object} item - Item
   * @return {Void}
   */
  selectItem(item: any = {}) {
    if (this.max == 1) {
      this.list = [];
    }
    this.list = this.list.filter((i) => { return i.id != item.id; });
    this.list.push(item);
  }

  /**
   * Delete item
   *
   * @param {Object} item - Item
   * @return {Void}
   */
  deleteItem(item: any = {}) {
    if (this.min == 1 && this.list.length == 1) {
      return;
    }
    this.list = this.list.filter((i) => { return i.id != item.id; });
  }

  /**
   * Update
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  update(data: any = {}) {

    if (this.list && data && this.objectsEqual(this.list, data)) {
      return;
    }

    // Define event
    this.onChange.emit({
      message: 'update',
      result: data
    });

    // Define list
    this.list = data;
  }

  parseBoolean(value) {
    if (value == 'false') {
      return false;
    } else if (value == 'true') {
      return true;
    } else {
      return value;
    }
  }

}
