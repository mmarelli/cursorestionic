import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CorePageComponent } from '../../core.page-component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent extends CorePageComponent implements OnInit {

  // Define output event
  @Output() onChange: any = new EventEmitter<any>();
  @Output() onLoad: any = new EventEmitter<any>();

  // Define inputs
  @Input() root: any;
  @Input() edit: any;
  @Input() view: any;
  @Input() list: any;
  @Input() mode: any;
  @Input() data: any = {};

  // Define fields array
  fields = [];

  // Define page path
  pagePath = this.pageService.getCurrentPage().path;

  // Define page service subscription
  pageServiceSubscription = this.pageService.pageEventListener.subscribe((data: any) => {
    if (data && data.event == 'refresh' && (data.path == this.pagePath || data.force)) {
      this.search();
    }
  });

  /**
   *  Emit Onload
   */
  ngOnInit() {
    this.onLoad.emit(
      this
    );
  }

  /**
   *  Page Event Un subscription
   */
  ngOnDestroy() {
    this.pageServiceSubscription.unsubscribe();
  }

  /**
   * Angular OnChanges event
   *
   * @return {Void}
   */
  ngOnChanges() {

    // Verify edit input
    this.edit = this.parseBoolean(this.edit || this.root?.edit || false);

    // Verify view input
    this.view = this.view || this.root?.view || {};

    // Verify list input
    this.list = this.list || this.root?.list || [];

    // Verify list mode
    this.mode = this.mode || [];

    // Define page data
    this.pageService.data[this.view.entity] = this.view.search || this.pageService.data[this.view.entity] || {};

    // Define data
    this.data = this.pageService.data[this.view.entity];

    // Define fields
    this.fields = [];
    for (let name in this.view.fields) {
      this.view.fields[name].name = name;
      this.fields.push(this.view.fields[name]);
    }

    // Verify if refresh is needed
    if (this.view && (this.view.forceRefresh || this.mode == 'relation')) {
      this.search();
    }

  }

  /**
   * Search
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  search(params: any = {}) {
    console.log(this.pagePath + ' ' + (this.view ? this.view.entity : '') + 'View.search()')

    if (!this.view || !this.view.entity) {
      return;
    }

    // Define filter
    let filter: any = {};

    // Define filter offset
    if (this.data.offset) {
      filter.offset = this.data.offset;
    } else {
      filter.offset = 0;
    }

    // Define filter limit
    if (this.data.limit) {
      filter.limit = this.data.limit;
    } else {
      filter.limit = 30;
    }

    // Define filter extra
    if (this.data.extra) {
      filter.extra = this.data.extra;
    }

    // Define filter where
    filter.where = {};

    // Define filter where from view where
    if (this.view.where) {
      filter.where = { ...filter.where, ...this.view.where };
    }

    // Define filter where from data where
    if (this.data.where) {
      filter.where = { ...filter.where, ...this.data.where };
    }

    // Define filter where from data query
    if (this.data.query && this.data.queryField) {
      filter.where = { ...filter.where, [this.data.queryField]: { like: '%' + this.data.query + '%' } };
    }

    // Clean where clause
    for (let field in this.data.where) {
      if (this.data.where[field] === null || this.data.where[field] === undefined) {
        delete this.data.where[field];
      }
    }

    // Define filter order
    if (this.data.order && this.data.orderField) {
      filter.order = { [this.data.orderField]: this.data.order };
    }

    // Verify list
    if (params.target) {
      this.list = this.list || [];
    } else {
      this.list = null;
    }

    // Verify change event
    if (params.target && params.type == 'ionChange') {
      filter.offset = 0;
    }

    // Verify refresh event
    if (params.target && params.type == 'ionRefresh') {
      filter.offset = 0;
    }

    // Verify infitinte event
    if (params.target && params.type == 'ionInfinite') {
      filter.offset = (this.list || []).length;
    }

    // Define url
    if (this.view && this.view.entity) {
      params.url = params.url || '/api/' + this.view.entity + '/';
    } else {
      return;
    }

    // Define page show loading
    this.pageService.showLoading({
      message: params.loadingMessage || 'page.showLoadingOnSearch.message',
      delay: params.target ? 3000 : 1500
    });

    // Define http get
    this.httpService.customGet(params.url, { filter }).then((result) => {

      // Define list
      this.list = [...(filter.offset > 0 ? this.list : []), ...result];

      // Define page show success
      this.pageService.showSuccess({
        message: params.successMessage || 'page.showSuccessOnSearch.message',
        result: result
      });

      // Define event
      this.onChange.emit({
        message: 'search',
        result: result
      });
    }).catch((error) => {

      // Define page show warning
      this.pageService.showWarning({
        message: params.warningMessage || 'page.showWarningOnSearch.message',
        error: error
      });

      // Define event
      this.onChange.emit({
        message: 'search',
        error: error
      });
    }).finally(() => {

      // Verify target complete
      if (params.target && params.target.complete) {
        params.target.complete();
      }


    });
  }

  parseBoolean(value) {
    if (value === 'false') {
      return false;
    } else if (value == 'true') {
      return true;
    } else {
      return value;
    }
  }

}
