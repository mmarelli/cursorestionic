import { Directive } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpService } from './http.service';
import { PageService } from './page.service';

@Directive()
export abstract class CorePageComponent {

  /**
   * Constructor
   *
   * @params {Object} httpService - HTTP service
   * @params {Object} pageService - Page service
   * @return {Void}
   */
  constructor(
    public httpService: HttpService,
    public pageService: PageService
  ) {
  
  }

  /**
   * Auth page
   *
   * @params {String} url - URL
   * @return {Boolean}
   */
  authPage(url: string): boolean {
    return this.pageService.authPage(url);
  }

  /**
   * Goto page
   *
   * @params {String} url - URL
   * @params {Object} state - State
   * @params {Object} options - Options
   * @return {Boolean}
   */
  gotoPage(url: string, state: any = {}, options: any = {}): boolean {
    return this.pageService.gotoPage(url, state, options);
  }

  /**
   * Goto back
   *
   * @return {Boolean}
   */
  gotoBack(): boolean {
    return this.pageService.gotoBack();
  }

  /**
   * Goto home
   *
   * @return {Boolean}
   */
  gotoHome(): boolean {
    return this.pageService.gotoHome();
  }

}
