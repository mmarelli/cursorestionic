import { Injectable } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Platform, NavController, ActionSheetController, AlertController, LoadingController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Config } from '../config';
import { HttpService } from './http.service';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  public config: any = Config;
  public auth: any;
  public data: any = {};
  public isLoading: any = false;

  public pageEvent = new Subject();
  public pageEventListener = this.pageEvent.asObservable();

  /**
   * Constructor
   *
   * @return {Void}
   */
  constructor(
    public router: Router,
    public platform: Platform,
    public navController: NavController,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public modalController: ModalController,
    public popoverController: PopoverController,
    public toastController: ToastController,
    public translateService: TranslateService,
    public nativeAudio: NativeAudio,
    public httpService: HttpService
  ) {
    this.httpService.getUserAsObservable().subscribe((result) => {
      this.auth = result || null;
    });
  }

  /*
   * Data methods
   */
  getCurrentPage() {
    let page = { ...this.router.routerState.snapshot.root.firstChild.data };
    let part = page.path.split('/')
    page.code = page.code || part[0];
    page.view = page.view || part[1];
    page.params = {
      ...(this.router.routerState.snapshot.root.firstChild.params || {}),
      ...(this.router.routerState.snapshot.root.firstChild.queryParams || {}),
      ...(history.state || {})
    };
    delete page.params.navigationId;
    return page;
  }

  getCurrentUser() {
    return this.auth;
  }

  /**
   * Auth page
   *
   * @params {String} url - URL
   * @return {Boolean}
   */
  authPage(url: string): boolean {
    for (let p in this.config.pages) {
      if (
        this.config.pages[p].path == url && 
        this.config.pages[p].auth && 
          (
            this.config.pages[p].auth.indexOf('all') > -1 || 
            (this.auth && this.auth.type == 'root' && !(this.config.pages[p].auth.indexOf('anonymous') > -1)) || 
            this.config.pages[p].auth.indexOf((this.auth || {}).type || 'anonymous') > -1
            )
        ) {
        return true;
      }
    }
    return false;
  }

  /**
   * Goto page
   *
   * @params {String} url - URL
   * @params {Object} state - State
   * @params {Object} options - Options
   * @return {Boolean}
   */
  gotoPage(url: string, state: any = {}, options: any = {}): boolean {
    for (let s in state) {
      url = url.replace(new RegExp(':' + s), state[s]);
    }
    if(!this.auth && this.config.route.forceSignIn) {
      this.navController.navigateRoot(this.config.route.signIn);
    } else {
      this.navController.navigateForward(url, { ...options, state: state });
    }
    return true;
  }

  /**
   * Goto back
   *
   * @return {Boolean}
   */
  gotoBack(): boolean {
    this.navController.back();
    return true;
  }

  /**
   * Goto home
   *
   * @return {Boolean}
   */
  gotoHome(): boolean {
    console.log(2)
    if(!this.auth && this.config.route.forceSignIn) {
      console.log(11)
      this.navController.navigateRoot(this.config.route.signIn);
    } else {
      console.log(22)
      this.navController.navigateRoot(this.config.route.home);
    }
    return true;
  }

  /**
   * Show loading
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  showLoading(params: any = {}) {
    this.showMessage({
      controller: 'loadingController',
      message: 'page.showLoading.message',
      name: 'page.showLoading.title',
      color: 'dark',
      delay: 0,
      duration: 10000,
      error: null,
      result: null,
      ...params
    });
  }

  /**
   * Show success
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  showSuccess(params: any = {}) {
    this.showMessage({
      controller: 'toastController',
      message: 'page.showSuccess.message',
      name: 'page.showSuccess.title',
      color: 'success',
      delay: 0,
      duration: 2000,
      error: null,
      result: null,
      ...params
    });
  }

  /**
   * Show warning
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  showWarning(params: any = {}) {
    this.showMessage({
      controller: 'toastController',
      message: 'page.showWarning.message',
      name: 'page.showWarning.title',
      color: 'danger',
      delay: 0,
      duration: 3000,
      error: null,
      result: null,
      ...params
    });
  }

  /**
   * Show Message
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  showMessage(params: any = {}) {

    // Define result
    let result = params;
    result.controller = result.controller || 'toastController';
    result.id = Math.random();

    // Verify message
    if (params.message) {
      let test = params.message;
      let text = this.translateService.instant(test);
      if (text && text != test) {
        result.message = text;
      } else {
        result.message = null;
      }
    }

    // Verify message from error
    if (params.error && params.error.error) {
      let test = params.error.error.message;
      let text = this.translateService.instant(test);
      if (text && text != test) {
        result.message = text;
      }
    }

    // Verify title
    if (params.title) {
      let test = params.title;
      let text = this.translateService.instant(test);
      if (text && text != test) {
        result.title = text;
      } else {
        result.title = null;
      }
    }

    // Remove message
    this.hideMessage();

    // Define message loading
    if (result.controller == 'loadingController') {
      this.isLoading = true;
    } else {
      this.isLoading = false;
    }

    // Define message
    if (result.message) {
      setTimeout(async () => {
        if (result.controller == 'loadingController' && !this.isLoading) {
          return;
        }
        await this[result.controller].create({
          buttons: result.buttons,
          message: result.message,
          header: result.title,
          color: result.color,
          duration: result.duration
        }).then((current) => {
          current.present().then(() => {
            if (result.controller == 'loadingController' && !this.isLoading) {
              current.dismiss().then(() => {
              }).catch(() => {
              });
            }
          }).catch(() => {
          });
        }).catch(() => {
        });
      }, (result.delay || 0));
    }
  }

  /**
   * Hide message
   *
   * @param {Object} params - Parameters
   * @return {Void}
   */
  hideMessage(params: any = {}) {
    if (this.isLoading) {
      this.isLoading = false;
      this.loadingController.dismiss().then(() => {
      }).catch((error) => {
      });
    }
  }


  /**
   * Format Form
   *
   * @param {Object} params - Parameters
   * @return {Object}
   */
  makeForm(fields: any, values: any): any {
    let item = {};
    for (let f in fields) {
      item[f] = (values && values[f] !== undefined ? values[f] : null) || (fields[f].value !== undefined ? fields[f].value : null);
    }
    let form = new FormGroup({});
    for (let i in item) {
      let pattern;
      if (fields[i].match && (typeof fields[i].match == 'string' || (typeof fields[i].match == 'object' && !Array.isArray(fields[i].match)))) {
        pattern = fields[i].match;
      } else {
        pattern = null
        // TODO MAX: ver q hace ese pattern, con enter no funciona
        //        pattern = '.*'
      }
      form.addControl(i, new FormControl(item[i], Validators.pattern(pattern)));
    }
    return form;
  }

  /**
   * Audio Support
   *
   */
  initAudio() {
    this.nativeAudio.preloadSimple('notification', 'assets/audio/notification.mp3');
  }

  /**
   * Play Audio
   *
   */
  playAudio(audio) {
    this.nativeAudio.play(audio);
  }
}
