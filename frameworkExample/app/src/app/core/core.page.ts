import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Directive } from '@angular/core';
import { CorePageComponent } from './core.page-component';

@Directive()
export abstract class CorePage extends CorePageComponent {

  // Define user
  public user: any = {};

  // Define page
  public page: any = {};

  /**
   * Ion view will enter
   *
   * @return {Void}
   */
  ionViewWillEnter() {
    this.user = this.pageService.getCurrentUser();
    this.page = this.pageService.getCurrentPage();
  }

  /**
   * Ion view did enter
   *
   * @return {Void}
   */
  ionViewDidEnter() {
    this.refreshPage();
  }

  /**
   * Ion view will leave
   *
   * @return {Void}
   */
  ionViewWillLeave() {
    this.pageService.hideMessage();
  }

  /**
  *  Fire Refresh event
  * 
  */
  refreshPage(force = false) {
    this.pageService.pageEvent.next({
      //      page: this.page.code, 
      //      view: this.page.view, 
      path: this.page.path,
      event: 'refresh',
      force: force
    })
  }

}
