import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDatetime'
})
export class FormatDatetimePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return (value || '').replace(/([0-9]{4})-([0-9]{2})-([0-9]{2}).([0-9]{2}):([0-9]{2})(.+)?/, '$3/$2/$1 $4:$5');
  }
}
