import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatPosition'
})
export class FormatPositionPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return value && value.coordinates ? value.coordinates[0] + ' (lat) ' + value.coordinates[1] + ' (lng)' : '';
  }
}
