import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDistance'
})
export class FormatDistancePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let k = Math.floor((parseFloat(value || 0) / 1000) * Math.pow(10, 2)) / Math.pow(10, 2);
    let m = Math.floor((parseFloat(value || 0) / 1) * Math.pow(10, 2)) / Math.pow(10, 2);
    return k > 1 ? k + 'km' : (m > 0 ? m + 'm' : '');
  }
}
