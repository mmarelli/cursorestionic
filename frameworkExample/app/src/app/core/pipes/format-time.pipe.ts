import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return (value || '').replace(/([0-9]{4})-([0-9]{2})-([0-9]{2}).([0-9]{2}):([0-9]{2})(.+)?/, '$4:$5');
  }
}
