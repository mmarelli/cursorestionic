import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatText'
})
export class FormatTextPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return (value?.toString() || '').trim().replace(/<[^>]*>?/gm, '').replace(/\s\s+/g, ' ');
  }
}
