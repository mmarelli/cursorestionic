import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatPhone'
})
export class FormatPhonePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return (value || '').toString().toLowerCase().trim();
  }
}
