import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatEmail'
})
export class FormatEmailPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      value = value.toString().toLowerCase().trim(); 
      //return '<a href="mailto:' + value + '">' + value + '</a>';‹
      return value;
    } else {
      return '';
    }

  }
}
