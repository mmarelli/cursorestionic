import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config } from '../config'

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private config: any = Config;
  private userK: string = 'user';
  private userV: any;
  private user$: BehaviorSubject<boolean> = new BehaviorSubject(null);

  /**
   * Constructor
   *
   * @params {Object} storage - Storage
   * @params {Object} platform - Platform
   * @params {Object} httpClient - HTTP client
   * @return {Void}
   */
  constructor(
    private storage: Storage,
    private platform: Platform,
    private httpClient: HttpClient
  ) {
    this.platform.ready().then(() => {
      this.verifyUser();
    });
  }

  /**
   * Verify user
   *
   * @return {Promise}
   */
  verifyUser(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get(this.userK).then((result) => {
        if (result) {
          this.user$.next(result);
          this.userV = result;
        }
        resolve(result);
      }).catch(reject);
    });
  }

  /**
   * Update user
   *
   * @return {Promise}
   */
  updateUser(user: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.set(this.userK, user).then((result) => {
        this.user$.next(result);
        this.userV = result;
        resolve(result);
      }).catch(reject);
    });
  }

  /**
   * Remove user
   *
   * @return {Promise}
   */
  removeUser(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.remove(this.userK).then((result) => {
        this.user$.next(false);
        this.userV = null;
        resolve(result);
      }).catch(reject);
    });
  }

  /**
   * Verify
   *
   * @return {Promise}
   */
  getUser(): any {
    return this.user$;
  }

  /**
   * Verify
   *
   * @return {Promise}
   */
  getUserAsObservable(): Observable<any> {
    return this.user$.asObservable();
  }

  /**
   * Verify
   *
   * @return {Promise}
   */
  getUserInMemory(): any {
    return this.userV;
  }

  /**
   * Custom URL
   *
   * @param {String} method - Method
   * @return {String}
   */
  customUrl(params: any): string {
    let uri = this.formatHttpUri(params.uri);
    let qs = this.formatHttpQs(params.qs);
    return this.config.page.url + uri + qs;
  }

  /**
   * Custom GET
   *
   * @param {String} uri - URI
   * @param {Object} qs - Query string
   * @return {Promise}
   */
  customGet(uri: string, qs: any = null): Promise<any> {
    return this.customRequest({
      method: 'GET',
      uri: uri,
      qs: qs
    });
  }

  /**
   * Custom PUT
   *
   * @param {String} uri - URI
   * @param {Object} body - Body
   * @return {Promise}
   */
  customPut(uri: string, body: any = {}): Promise<any> {
    return this.customRequest({
      method: 'PUT',
      uri: uri,
      body: body
    });
  }

  /**
   * Custom POST
   *
   * @param {String} uri - URI
   * @param {Object} body - Body
   * @return {Promise}
   */
  customPost(uri: string, body: any = {}): Promise<any> {
    return this.customRequest({
      method: 'POST',
      uri: uri,
      body: body
    });
  }

  /**
   * Custom DELETE
   *
   * @param {String} uri - URI
   * @return {Promise}
   */
  customDelete(uri: string): Promise<any> {
    return this.customRequest({
      method: 'DELETE',
      uri: uri
    });
  }

  /**
   * Custom request
   *
   * @param {Object} params - Parameters
   * @return {Promise}
   */
  customRequest(params: any): Promise<any> {
    return new Promise((resolve, reject) => {

      // Format parameters
      let method = this.formatHttpMethod(params.method);
      let uri = this.formatHttpUri(params.uri);
      let qs = this.formatHttpQs(params.qs);
      let body = this.formatHttpBody(params.body);
      let headers = this.formatHttpHeaders(params.headers);

      // Define request
      this.httpClient.request(method, this.config.page.url + uri + qs, {
        body: body,
        headers: new HttpHeaders(headers)
      }).subscribe(async (result: any) => {

        // Verify result
        if (result && typeof result == 'object' && !Array.isArray(result) && result['token'] !== undefined) {
          if (result['token']) {
            await this.updateUser(result);
          } else {
            await this.removeUser();
          }
        }

        // Return result
        resolve(result);
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Format HTTP method
   *
   * @param {String} method - Method
   * @return {String}
   */
  private formatHttpMethod(method: any): string {
    return (method || 'GET').trim().toUpperCase();
  }

  /**
   * Format HTTP URI
   *
   * @param {String} uri - URI
   * @return {String}
   */
  private formatHttpUri(uri: any): string {
    return (uri || '/').trim();
  }

  /**
   * Format HTTP QS
   *
   * @param {Object} qs - Query string
   * @return {String}
   */
  private formatHttpQs(qs: any): string {
    return qs ? '?' + this.formatObjectToHttpQs(qs) : '';
  }

  /**
   * Format HTTP body
   *
   * @param {Object} body - Body
   * @return {Object}
   */
  private formatHttpBody(body: any): any {
    return body;
  }

  /**
   * Format HTTP headers
   *
   * @param {Object} headers - Headers
   * @return {Object}
   */
  private formatHttpHeaders(headers: any): any {

    // Define result
    headers = headers || {};

    // Verify access token
    if (!headers['x-access-token'] && this.userV && this.userV.token) {
      headers['x-access-token'] = this.userV.token;
    }

    // Verify content type
    if (!headers['x-content-type'] && !headers['content-type']) {
      headers['content-type'] = 'application/json; charset=UTF-8';
    }

    // Verify headers
    for (let h in headers) {
      if (!headers[h]) {
        delete headers[h];
      }
    }

    // Return result
    return headers;
  }

  /**
   * Format object to HTTP QS
   *
   * @param {Object} body - Body
   * @return {Object}
   */
  private formatObjectToHttpQs(params: any, prefix: any = '') {

    // Define result
    let result = [];

    // Create result items
    for (let p in params) {
      if (p && params.hasOwnProperty(p)) {
        let k = prefix ? prefix + '[' + p + ']' : p;
        let v = params[p];
        if (typeof v === 'object') {
          let d = this.formatObjectToHttpQs(v, k);
          if (d) {
            result.push(d);
          }
        } else {
          result.push(encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
      }
    }

    // Return result
    return result.join('&');
  }
}
