# CURSO INDUCCION MAXMOBILE

## PROGRAMA

### SO

WINDOWS / MACOS / LINUX

### BACK END:

API REST BACK END: API REST

Javascript
NodeJS
Epress
Sequelize
JWT

### FRONT END:

APP WEB / IOS / ANDROIDFRONT END: APP WEB / IOS / ANDROID

HTML5
CSS3
Javascript
Typescript
SCSS
Angular 9
Ionic 5 (cordova - phonegap)

### DEV OPS:

Cloudflare
AWS
AWS EC2
AWS S3
AWS RDS
AWS EB

## CODIGO FUENTE

Para descargar este repo teniendo instalado minimamente git y nodejs

1_ git clone https://mmarelli@bitbucket.org/mmarelli/cursorestionic.git
2_ para ejecutar el API

*cd api
npm i
npm start*

3_ para ejecutar la APP

*cd app
npm i
ionic serve*

## DESCARGAS

[https://code.visualstudio.com/](https://)

[https://mariadb.org/](https://)

[https://git-scm.com/](https://)

[https://www.sourcetreeapp.com/](https://)

[https://www.mysql.com/products/workbench/](https://)

[https://nodejs.org/](https://)

[https://www.postman.com/downloads/](https://)

## HERRAMIENTAS WEB

[https://bitbucket.org/](https://)

[https://trello.com/](https://)

[https://www.atlassian.com/software/jira](https://)

## OTROS LINKS

[https://aws.amazon.com/es/](https://)

[https://www.npmjs.com/](https://)

[https://github.com/](https://)

[https://ionicframework.com/](https://)

[https://angular.io/](https://)

[https://www.typescriptlang.org/](https://)

[https://sass-lang.com/](https://)

[https://cordova.apache.org/](https://)

## LIBS NODEJS

[https://expressjs.com/es/](https://)

[https://sequelize.org/master/](https://)

[https://jwt.io/](https://)

## TUTORIALES:

### HTML

[https://developer.mozilla.org/en-US/docs/Learn/HTML](https://)

### CSS

[https://developer.mozilla.org/en-US/docs/Learn/CSS](https://)

### JavaScript

[https://developer.mozilla.org/en-US/docs/Web/JavaScript](https://)

### TypeScript

[https://www.typescriptlang.org/docs/handbook/intro.html](https://)

### SASS

[https://sass-lang.com/guide](https://)

### ANGULAR

[https://angular.io/docs](https://)

### IONIC
[https://ionicframework.com/docs](https://)

### NODEJS

[https://nodejs.org/en/docs/guides/](https://)

### EXPRESS

[https://expressjs.com/es/starter/installing.html](https://)

### SEQUELIZE

[https://sequelize.org/master/manual/getting-started.html](https://)

### REST+NODEJS+EXPRESS+SEQUELIZE+MYSQL

[https://bezkoder.com/node-js-express-sequelize-mysql/](https://)

### HTML5 + CSS3 + JS

[https://www.udemy.com/course/crea-una-pagina-web-moderna-con-html-css-y-javascript/](https://)

### ANGULAR

[https://www.udemy.com/course/curso-basico-de-angular-11-empieza-con-este-framework-js/](https://)

### AVANZADO - FRAMEWORK NPM

[https://devdactic.com/build-ionic-library-npm/](https://)
